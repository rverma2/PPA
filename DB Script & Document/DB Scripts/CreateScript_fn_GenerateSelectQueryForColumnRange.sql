USE [PPASurvey_DBProd]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GenerateSelectQueryForColumnRange]    Script Date: 7/26/2017 6:07:21 PM ******/
DROP FUNCTION [dbo].[FN_GenerateSelectQueryForColumnRange]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GenerateSelectQueryForColumnRange]    Script Date: 7/26/2017 6:07:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*-- =============================================
-- Author:Rahul Verma
-- Create date: 20 July 2017
-- Description:	This function is used to generate select queries for column ranges.
				Input Parameters are as Follows:
				@start_question: Start column name from where the select queries will be generated
				@end_question: End column name from where the select queries will be generated
			
- Execution Sample: select [dbo].[FN_GenerateSelectQueryForColumnRange]('Q64a','Q66k')
-- =============================================*/


CREATE FUNCTION [dbo].[FN_GenerateSelectQueryForColumnRange] (@start_question nvarchar(5), @end_question nvarchar(5))
RETURNS NVARCHAR(2000)
AS BEGIN
 
DECLARE @pos INT
DECLARE @len INT

DECLARE @value varchar(10)
DECLARE @columns varchar(5000)
DECLARE @SQLString varchar(5000)

DECLARE @start_ordinal int, @end_ordinal int

set @start_ordinal = (select ORDINAL_POSITION from INFORMATION_SCHEMA.COLUMNS 
					   WHERE TABLE_NAME = 'Source.InputDataBenchMarkSource'
					   and COLUMN_NAME = @start_question)



set @end_ordinal = (select ORDINAL_POSITION from INFORMATION_SCHEMA.COLUMNS 
					   WHERE TABLE_NAME = 'Source.InputDataBenchMarkSource'
					   and COLUMN_NAME = @end_question)


SELECT @columns = ISNULL(@columns + ', ','') + QUOTENAME(column_name)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Source.InputDataBenchMarkSource' AND COLUMN_NAME <> 'RowId'
AND ORDINAL_POSITION >= @start_ordinal AND ORDINAL_POSITION <= @end_ordinal+1
ORDER BY ORDINAL_POSITION


set @SQLString = ' (';

set @pos = 0
set @len = 0

WHILE CHARINDEX(',', @columns, @pos+1)>0
BEGIN
set @len = CHARINDEX(',', @columns, @pos+1) - @pos
set @value = SUBSTRING(@columns, @pos, @len)
   
set @SQLString = @SQLString+' isnull(CAST('+@value+' AS tinyint),0) +'

set @pos = CHARINDEX(',', @columns, @pos+@len) +1

END

set @SQLString = LEFT(@SQLString, len(@SQLString)-1)

set @SQLString = @SQLString+ ')'
    RETURN @SQLString

END






GO


