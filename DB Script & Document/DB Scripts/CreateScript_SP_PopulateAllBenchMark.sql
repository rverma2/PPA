USE [PPASurvey_DBProd]
GO

/****** Object:  StoredProcedure [dbo].[SP_PopulateAllBenchmarks]    Script Date: 7/27/2017 4:54:55 PM ******/
DROP PROCEDURE [dbo].[SP_PopulateAllBenchmarks]
GO

/****** Object:  StoredProcedure [dbo].[SP_PopulateAllBenchmarks]    Script Date: 7/27/2017 4:54:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*-- =============================================
-- Author:Rahul Verma
-- Create date: 19 July 2017
-- Description:  This Proc calculates and populates benchmarks for all existing look up tables whoes logic is available
				Input Parameters are as Follows:
				@LookupName:Lookup Table for which Benchmark,Percentile Values, has to be calculated
				@Year:The Year for which data Points have to be picked.
				@type:BenchmarkType,If 'F'(Full) then all historical data points  including the @year will be considered
				else IF 'A'(Annual) then Data points only for only that @year will be considered.


- Different case scenarios:

  To populate all lookups:
- Execution Sample:Exec dbo.SP_PopulateAllBenchmarks NULL,'2017','F'

To populate a single annual lookup:
- Execution Sample:Exec dbo.SP_PopulateAllBenchmarks 'Lookup.AverageDirectPayExamFee','2017','A'
-- =============================================*/



CREATE PROCEDURE [dbo].[SP_PopulateAllBenchmarks] (@LookupName sysname = NULL, @year varchar(4) = NULL, @type char(1))

AS
BEGIN

  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @Table_Name nvarchar(255),
          @ExecutionDetailLookupTableAnnual nvarchar(255),
          @CalculateBenchmarkReturnValue int
  DECLARE @MINROWID int,
          @MaxROWID int,
          @IdentityValueParent int,
          @IdentityValueChild int
  DECLARE @TargetBenchmarkCount tinyint,
          @SuccessBenchmarkCount tinyint,
          @FailedBenchmarkCount tinyint
  DECLARE @columns varchar(8000)


  BEGIN TRY

   IF (@type <> 'F' AND  @type <> 'A')
   BEGIN

   PRINT 'Benchmark type must be either ''F'' (Full) or ''A'' (Annual)'

   RETURN -1

   END
    IF (@type IS NULL
      OR @type = '')
    BEGIN
      SET @type = 'F'
    END

    IF (@type = 'A')
    BEGIN

      IF (@LookupName IS NULL
        OR @LookupName = ''
        OR @year IS NULL
        OR @year = '')
      BEGIN

        PRINT 'Lookup name and Year cannot be blank for type = A'

        RETURN -1

      END

    END


    IF OBJECT_ID('tempdb..##InputDataBenchMarkSourceYear', 'U') IS NOT NULL
      DROP TABLE ##InputDataBenchMarkSourceYear

    IF OBJECT_ID('tempdb..#LookUpTableNames', 'U') IS NOT NULL
      DROP TABLE #LookUpTableNames



    SELECT
      @columns = ISNULL(@columns + ', ', '') + QUOTENAME(column_name)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Source.InputDataBenchMarkSource'
    AND COLUMN_NAME <> 'RowId'
    ORDER BY ORDINAL_POSITION


    --Preparing data set on basis of Input parameter @year & @type
    IF @type = 'f'
    BEGIN


      EXEC ('SELECT IDENTITY(INT,1,1) AS ID, ' + @columns + ' into ##InputDataBenchMarkSourceYear FROM [Source.InputDataBenchMarkSource] ' + 'where YearID IN ( SELECT YEARID FROM YEARMASTER WHERE ActiveYear <=' + @year + '-1 )')

    END

    IF @type = 'a'
    BEGIN

      EXEC ('SELECT IDENTITY(INT,1,1) AS ID, ' + @columns + ' into ##InputDataBenchMarkSourceYear FROM [Source.InputDataBenchMarkSource] ' + 'where YearID IN ( SELECT YEARID FROM YEARMASTER WHERE ActiveYear =' + @year + '-1 )')
    END

    --select * from ##InputDataBenchMarkSourceYear

	IF NOT EXISTS (SELECT 1 FROM ##InputDataBenchMarkSourceYear)
	BEGIN

	 PRINT 'Data is not available for year '+@year
	 RETURN -1

	END



    CREATE TABLE #LookUpTableNames (
      ROWID int IDENTITY (1, 1) PRIMARY KEY,
      LookUpTableName nvarchar(255)
    )



    IF (@type = 'F')
    BEGIN
      INSERT INTO #LookUpTableNames
        SELECT
          TABLE_NAME
        FROM INFORMATION_SCHEMA.TABLES
        WHERE LEFT(RIGHT(TABLE_NAME, 5), 1) <> '_'
        AND TABLE_NAME LIKE '%lookup%'

    END

    IF (@type = 'A')
    BEGIN

      INSERT INTO #LookUpTableNames (LookUpTableName)
        VALUES (@LookupName)

      SET @ExecutionDetailLookupTableAnnual = @LookupName + '_' + @year;

    END

    --select * from #LookUpTableNames


    SELECT
      @MINROWID = MIN(ROWID),
      @MaxROWID = MAX(ROWID)
    FROM #LookUpTableNames

    INSERT INTO [ExecutionSummary] (ExecutionSummaryStartDate, BenchmarkYear, ExecutionBenchmarkType)
      VALUES (GETDATE(), @year, UPPER(@type))

    SELECT
      @IdentityValueParent = SCOPE_IDENTITY()

    WHILE (@MINROWID IS NOT NULL
      AND @MINROWID <= @MaxROWID)
    BEGIN

      SELECT
        @Table_Name = LookUpTableName
      FROM #LookUpTableNames
      WHERE ROWID = @MINROWID

    BEGIN TRY


      IF (@type = 'F')
        INSERT INTO ExecutionDetail (ExecutionSummaryId, ExecutionDetailLookupTable, ExecutionDetailStartDate)
          VALUES (@IdentityValueParent, @Table_Name, GETDATE())

      IF (@type = 'A')
        INSERT INTO ExecutionDetail (ExecutionSummaryId, ExecutionDetailLookupTable, ExecutionDetailStartDate)
          VALUES (@IdentityValueParent, @ExecutionDetailLookupTableAnnual, GETDATE())


      SELECT
        @IdentityValueChild = SCOPE_IDENTITY();

      SET @CalculateBenchmarkReturnValue = 0;

      EXEC SP_CalculateBenchMark @Table_Name,
                                 @year,
                                 @type,
                                 @CalculateBenchmarkReturnValue OUTPUT


      IF (@CalculateBenchmarkReturnValue = -1)
      BEGIN

        -- case of no benchmark values based on the lookup value
        UPDATE ExecutionDetail
        SET ExecutionDetailLookupRecordCount = 0,
            ExecutionDetailEndDate = GETDATE(),
            ExecutionDetailStatus = 'S',
			 ExecutionErrorDescription = 'Benchmark Data Not Available'
        WHERE ExecutionDetailID = @IdentityValueChild

      END

      ELSE
      IF (@CalculateBenchmarkReturnValue = -2)

      BEGIN

        -- case of logic not present in SP_BenchMarkLogic based on the lookup value

        UPDATE ExecutionDetail
        SET ExecutionDetailLookupRecordCount = 0,
            ExecutionDetailEndDate = GETDATE(),
            ExecutionDetailStatus = 'F',
            ExecutionErrorDescription = 'Benchmark Logic Not Available'
        WHERE ExecutionDetailID = @IdentityValueChild

      END

      ELSE

      BEGIN

        IF (@type = 'F')
        BEGIN

          EXEC ('UPDATE ExecutionDetail SET ExecutionDetailLookupRecordCount = (SELECT COUNT(*) FROM [' + @Table_Name + ']), ExecutionDetailEndDate = GETDATE(), ExecutionDetailStatus=''S'' WHERE ExecutionDetailID = ' + @IdentityValueChild)

        END

        IF (@type = 'A')
        BEGIN

          EXEC ('UPDATE ExecutionDetail SET ExecutionDetailLookupRecordCount = (SELECT COUNT(*) FROM [' + @ExecutionDetailLookupTableAnnual + ']), ExecutionDetailEndDate = GETDATE(), ExecutionDetailStatus=''S'' WHERE ExecutionDetailID = ' + @IdentityValueChild)

        END

      END


    END TRY

    BEGIN CATCH
      /****************************************************************/
      /* RAISE ERROR							*/
      /****************************************************************/
      DECLARE @ErrorMessage nvarchar(1000),
              @ErrorMessagetext nvarchar(1000),
              @ErrorSeverity int,
              @ErrorState int,
              @ErrorNumber int;


      SELECT
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorMessagetext =
                           CASE
                             WHEN ERROR_PROCEDURE() IS NULL THEN 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Sql in Procedure: ' + ISNULL(OBJECT_NAME(@@PROCID), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                             ELSE 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Procedure: ' + ISNULL(ERROR_PROCEDURE(), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                           END,
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();


      UPDATE ExecutionDetail
      SET ExecutionDetailLookupRecordCount = 0,
          ExecutionDetailEndDate = GETDATE(),
          ExecutionErrorDescription = ERROR_MESSAGE(),
          ExecutionDetailStatus = 'F'
      WHERE ExecutionDetailID = @IdentityValueChild
	        
    END CATCH

      SET @MINROWID = @MINROWID + 1

    END

    SELECT
      @TargetBenchmarkCount = COUNT(1)
    FROM ExecutionDetail
    WHERE ExecutionSummaryId = @IdentityValueParent;
    SELECT
      @SuccessBenchmarkCount = COUNT(1)
    FROM ExecutionDetail
    WHERE ExecutionSummaryId = @IdentityValueParent
    AND ExecutionDetailStatus = 'S';
    SELECT
      @FailedBenchmarkCount = COUNT(1)
    FROM ExecutionDetail
    WHERE ExecutionSummaryId = @IdentityValueParent
    AND ExecutionDetailStatus = 'F';

    UPDATE ExecutionSummary
    SET ExecutionSummaryEndDate = GETDATE(),
        TargetBenchmarkCount = @TargetBenchmarkCount,
        PopulatedBenchmarkCount = @SuccessBenchmarkCount,
        ExecutionSummaryStatus = (CASE
          WHEN @TargetBenchmarkCount = @FailedBenchmarkCount THEN 'F'
          WHEN @TargetBenchmarkCount = @SuccessBenchmarkCount THEN 'S'
          ELSE 'P'
        END)
    WHERE ExecutionSummaryId = @IdentityValueParent

    IF OBJECT_ID('tempdb..##InputDataBenchMarkSourceYear', 'U') IS NOT NULL
      DROP TABLE ##InputDataBenchMarkSourceYear

    IF OBJECT_ID('tempdb..#LookUpTableNames', 'U') IS NOT NULL
      DROP TABLE #LookUpTableNames


  END TRY


  BEGIN CATCH
    /****************************************************************/
    /* RAISE ERROR							*/
    /****************************************************************/
   SELECT
      @ErrorNumber = ERROR_NUMBER(),
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorMessagetext =
                         CASE
                           WHEN ERROR_PROCEDURE() IS NULL THEN 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Sql in Procedure: ' + ISNULL(OBJECT_NAME(@@PROCID), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                           ELSE 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Procedure: ' + ISNULL(ERROR_PROCEDURE(), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                         END,
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();


    UPDATE ExecutionDetail
    SET ExecutionDetailEndDate = GETDATE(),
        ExecutionErrorDescription = ERROR_MESSAGE(),
        ExecutionDetailStatus = 'F'
    WHERE ExecutionDetailID = @IdentityValueChild

    RAISERROR (@ErrorMessagetext,
    @ErrorSeverity,
    @ErrorState);

    IF OBJECT_ID('tempdb..##InputDataBenchMarkSourceYear', 'U') IS NOT NULL
      DROP TABLE ##InputDataBenchMarkSourceYear

    IF OBJECT_ID('tempdb..#LookUpTableNames', 'U') IS NOT NULL
      DROP TABLE #LookUpTableNames

  END CATCH


END
GO


