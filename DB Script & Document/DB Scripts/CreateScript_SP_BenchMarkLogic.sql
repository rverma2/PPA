USE [PPASurvey_DBProd]
GO

/****** Object:  StoredProcedure [dbo].[SP_BenchMarkLogic]    Script Date: 7/27/2017 4:56:01 PM ******/
DROP PROCEDURE [dbo].[SP_BenchMarkLogic]
GO

/****** Object:  StoredProcedure [dbo].[SP_BenchMarkLogic]    Script Date: 7/27/2017 4:56:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-- =============================================
-- Author:Rahul Verma
-- Create date: 07 July 2017
-- Description:  This Proc returns sorted values of data Points in ascending order on the basis of benchmark logic
				,calculation of data points for a Metrics, on which percentile values are calculated.
				Input Parameters are as Follows:
				@LookupName:Lookup Table for which Benchmark,Percentile Values, has to be calculated
				@Year:The Year for which data Points have to be picked.
				@type:BenchmarkType,If 'F'(Full) then all historical data points  including the @year will be considered
				else IF 'A'(Annual) then Data points only for only that @year will be considered.
11 July 2017:Shantanu Sharam modified Proc to add more Benchmark logics.
24 July 2017:Shantanu Sharam modified Proc to add more Benchmark logics for expense ratio by categories
-- Execution Sample:Exec dbo.SP_BenchMarkLogic 'Lookup.AverageDirectPayExamFee_J','2013','F'
-- =============================================*/


CREATE PROCEDURE [dbo].[SP_BenchMarkLogic] (@LookupName sysname, @year varchar(4), @type char(1))

AS
BEGIN

  --SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @query_part nvarchar(4000) = '';
  DECLARE @LookupLogicExists bit = 0;


  BEGIN TRY



    CREATE TABLE #BenchmarkRawData (
      BenchmarkCandidate decimal(18, 2)
    )
    ----Logic to get values as candidate keys on which benchmark will be calculated 


    IF @LookupName = 'Lookup.PercentOfGrossRevenueVSPPayments_J'
    BEGIN

      SET @LookupLogicExists = 1

	  
      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q27b, 0) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.PercentOfGrossRevenueFromMedicarePayments_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q27, 0) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.AvgCollectFeeRevPerCompl'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q26, 0) / (Q14))) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END


    IF @LookupName = 'Lookup.ExamFeeNonCL'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q47 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q47 IS NOT NULL

    END


    IF @LookupName = 'Lookup.ExamFeeSoftNewFitSPHERE'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q48 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q48 IS NOT NULL

    END



    IF @LookupName = 'Lookup.ExamFeeSoftNewFitTORIC'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q49 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q49 IS NOT NULL

    END


    IF @LookupName = 'Lookup.ExamFeeSoftNewFitMULTIFO'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q50 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q50 IS NOT NULL

    END


    IF @LookupName = 'Lookup.ExamFeeSoftLensNOREFITT'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q51 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q51 IS NOT NULL

    END


    IF @LookupName = 'Lookup.EyewearRxPer100ComplExam'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q28, 0) + ISNULL(Q29, 0)) / (Q14 / 100)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END

    IF @LookupName = 'Lookup.EyewearSalePercentageOfGrossRev'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q26f, 0) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END


    IF @LookupName = 'Lookup.GrossRevPerEyewearRx'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q26f, 0)) / (ISNULL(Q28, 0) + ISNULL(Q29, 0))) AS decimal(18, 2)) AS BenchmarkCandidate

        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(ISNULL(Q28, 0) + ISNULL(Q29, 0) AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(ISNULL(Q28, 0) + ISNULL(Q29, 0) AS decimal(18, 2))

        END <> 0

    END


    IF @LookupName = 'Lookup.EyewearGrossProfitMargin'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((((Q26f) - (ISNULL(Q52a, 0) + ISNULL(Q52b, 0) + ISNULL(Q52c, 0) + ISNULL(Q52d, 0) + ISNULL(Q52e, 0))) / (Q26f)) * 100) AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q26f IS NOT NULL
        AND Q26f <> 0

    END


    IF @LookupName = 'Lookup.OpticalDispensaryPercentOfTotalOfficeSpace_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q3, 0) / (Q2)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q2 IS NOT NULL
        AND Q2 <> 0

    END


    IF @LookupName = 'Lookup.MultipleEyewearPurchasePercent'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q30 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q30 IS NOT NULL

    END



    IF @LookupName = 'Lookup.ProgressiveLensAndPresbyopRx'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(((((ISNULL(Q31b, 0) / 100) * (ISNULL(Q28, 0) + ISNULL(Q29, 0)) * (ISNULL(Q32c, 0) / 100)) / (((ISNULL(Q31b, 0) / 100)) * (ISNULL(Q28, 0) + ISNULL(Q29, 0)))) * 100) AS decimal(18, 2))

          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE


          WHEN CAST((((ISNULL(Q31b, 0) / 100)) * (ISNULL(Q28, 0) + ISNULL(Q29, 0))) AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST((((ISNULL(Q31b, 0) / 100)) * (ISNULL(Q28, 0) + ISNULL(Q29, 0))) AS decimal(18, 2))

        END <> 0

    END



    IF @LookupName = 'Lookup.NoGlareLensPercentSpecLensRx'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33b AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33b IS NOT NULL

    END


    IF @LookupName = 'Lookup.HighIndexLensPercentSpecLensRx'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33a AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33a IS NOT NULL

    END


    IF @LookupName = 'Lookup.PhotochrLensPercentofSpecLensRx'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33c AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33c IS NOT NULL

    END


    IF @LookupName = 'Lookup.PrescriptionSunwearPercentofEyeWearRxes_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33d AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33d IS NOT NULL

    END


    IF @LookupName = 'Lookup.ComputerLensesPercentofEyeWearRxes_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33e AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33e IS NOT NULL

    END



    IF @LookupName = 'Lookup.OtherProductSales_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q26h AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q26h IS NOT NULL

    END


    IF @LookupName = 'Lookup.AverageFramesMarkUp_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q52a AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q52a IS NOT NULL

    END


    IF @LookupName = 'Lookup.FramesAvgWholesaleCostPerFrame_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q36 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q36 IS NOT NULL

    END


    IF @LookupName = 'Lookup.SiliconeHydroLensWearPercentSoft'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q41a AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q41a IS NOT NULL

    END


    IF @LookupName = 'Lookup.DailyDisposableLensPercentSoft'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q39a AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q39a IS NOT NULL

    END


    IF @LookupName = 'Lookup.SoftToricPercentSoftLens'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q40b AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q40b IS NOT NULL

    END


    IF @LookupName = 'Lookup.SoftMultiFocPercentSoftLens'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q40d AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q40d IS NOT NULL

    END


    IF @LookupName = 'Lookup.RGPLensWearerPercentOfCLWeares_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q40e AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q40e IS NOT NULL

    END


    IF @LookupName = 'Lookup.CLRefitPercentCLExam'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q43a AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q43a IS NOT NULL

    END


    IF @LookupName = 'Lookup.CLWearerPercentActivePatients'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q13b AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q13b IS NOT NULL

    END


    IF @LookupName = 'Lookup.CLSalesPercentGrossRev'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q26g, 0) / (Q24)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END


    IF @LookupName = 'Lookup.AnnCLSalesPerCLExam'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q26g, 0) / (Q15b)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q15b IS NOT NULL
        AND Q15b <> 0

    END

    IF @LookupName = 'Lookup.GrossRevenuePerCompleteExam'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(ISNULL(Q24, 0) / Q14 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END


    IF @LookupName = 'Lookup.CompleteExamsPerODHour'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(ISNULL(Q14, 0) / Q11 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q11 IS NOT NULL
        AND Q11 <> 0

    END


    IF @LookupName = 'Lookup.GrossRevPerNonODStaffHr'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(ISNULL(Q24, 0) / Q7 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q7 IS NOT NULL
        AND Q7 <> 0

    END


    IF @LookupName = 'Lookup.GrossRevenuePerODHour'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(ISNULL(Q24, 0) / Q11 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q11 IS NOT NULL
        AND Q11 <> 0

    END


    IF @LookupName = 'Lookup.CompleteExamsPer100Active'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q14, 0) / Q12) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q12 IS NOT NULL
        AND Q12 <> 0

    END


    IF @LookupName = 'Lookup.GrossRevPerActivePatient'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q24, 0) / Q12) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q12 IS NOT NULL
        AND Q12 <> 0

    END


    IF @LookupName = 'Lookup.PercentofCompleteEyeExamsByEyeGlass_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q15a, 0) / Q14) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END


    IF @LookupName = 'Lookup.PercentofCompleteEyeExamsByCLExams_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q15b, 0) / Q14) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END


    IF @LookupName = 'Lookup.PercentofCompleteEyeExamsByHealthyEyeExams_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q15c, 0) / Q14) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END


    IF @LookupName = 'Lookup.MedicalEyeCareVisitPercentTotal'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST(((ISNULL(Q20a, 0) + ISNULL(Q20b, 0) + ISNULL(Q20c, 0) + ISNULL(Q20d, 0) + ISNULL(Q20e, 0) + ISNULL(Q20f, 0) + ISNULL(Q20g, 0)) / (ISNULL(Q20a, 0) + ISNULL(Q20b, 0) + ISNULL(Q20c, 0) + ISNULL(Q20d, 0) + ISNULL(Q20e, 0) + ISNULL(Q20f, 0) + ISNULL(Q20g, 0) + ISNULL(Q14, 0))) * 100 AS decimal(18, 2))

          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(ISNULL(Q20a, 0) + ISNULL(Q20b, 0) + ISNULL(Q20c, 0) + ISNULL(Q20d, 0) + ISNULL(Q20e, 0) + ISNULL(Q20f, 0) + ISNULL(Q20g, 0) + ISNULL(Q14, 0) AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(ISNULL(Q20a, 0) + ISNULL(Q20b, 0) + ISNULL(Q20c, 0) + ISNULL(Q20d, 0) + ISNULL(Q20e, 0) + ISNULL(Q20f, 0) + ISNULL(Q20g, 0) + ISNULL(Q14, 0) AS decimal(18, 2))

        END <> 0

    END


    IF @LookupName = 'Lookup.AnnMedEyeCareVisitPer1000'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData


        SELECT
          CAST((ISNULL(Q20a, 0) + ISNULL(Q20b, 0) + ISNULL(Q20c, 0) + ISNULL(Q20d, 0) + ISNULL(Q20e, 0) + ISNULL(Q20f, 0) + ISNULL(Q20g, 0)) / (ISNULL(Q12, 0) / 1000) AS decimal(18, 2))

          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(ISNULL(Q12, 0) / 1000 AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(ISNULL(Q12, 0) / 1000 AS decimal(18, 2))

        END <> 0

    END


    IF @LookupName = 'Lookup.PercentExamsProvideWMangCareDis'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          Q18 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q18 IS NOT NULL

    END


    IF @LookupName = 'Lookup.PercentOfGrossRevenueDirectPatientPayments_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q27d, 0) / Q24) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END


    IF @LookupName = 'Lookup.PercentOfGrossRevenueAllHealthVisionPlans_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData

        SELECT
          CAST((ISNULL(Q27a, 0) / Q24) * 100 AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.CLNewFitsPer100CLExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q42a, 0) / (Q15b / 100))) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q15b IS NOT NULL
        AND Q15b <> 0

    END

    IF @LookupName = 'Lookup.CLGrossProfitMargin'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((((Q26g - ISNULL(Q52f, 0)) / (Q26g)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q26g IS NOT NULL
        AND Q26g <> 0

    END

    IF @LookupName = 'Lookup.PercentPatientsCLExamPurchEyewea'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q37 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q37 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ChairCostPerComplExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((((ISNULL(Q53, 0) + ISNULL(Q54, 0) + ISNULL(Q55, 0) + ISNULL(Q56, 0) + ISNULL(Q57, 0) + ISNULL(Q58, 0) + ISNULL(Q59, 0) + ISNULL(Q60, 0)) / (Q14))) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END

    IF @LookupName = 'Lookup.AnnualOccupancyCostperSquareFoot_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q54, 0) / (Q2))) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q2 IS NOT NULL
        AND Q2 <> 0

    END

    IF @LookupName = 'Lookup.NetIncomePercentGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((Q24 - (ISNULL(Q52j, 0) + ISNULL(Q53, 0) + ISNULL(Q54, 0) + ISNULL(Q55, 0) + ISNULL(Q56, 0) + ISNULL(Q57, 0) + ISNULL(Q58, 0) + ISNULL(Q59, 0) + ISNULL(Q60, 0))) / (Q24)) * 100 AS decimal(18, 2))

          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.AnnMrktSpendPerComplExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q56, 0) / (Q14)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END

    IF @LookupName = 'Lookup.AcctRecDaysOutstanding'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q25, 0) / (Q24 / 12)) * 30) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.ComputerLensesPercentofEyeWearRxes_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q33e AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q33e IS NOT NULL

    END

    ---PPA benchmark data that is not used in Keymetrics and Spectacles
    IF @LookupName = 'Lookup.OccupancyExpensePercentGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q54, 0) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.NonRefrFeePercentGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((((ISNULL(Q26b, 0) + ISNULL(Q26c, 0)) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END
    IF @LookupName = 'Lookup.NewPatientExamPercentTotalExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q16 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q16 IS NOT NULL

    END
    IF @LookupName = 'Lookup.MrktSpendPercentGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q56, 0) / (Q24)) * 100) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END
    IF @LookupName = 'Lookup.MonthlySoftLensPercentWearers'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q39c AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q39c IS NOT NULL

    END
    IF @LookupName = 'Lookup.GrossRevPerSqFt'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q24, 0) / (Q2)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q2 IS NOT NULL
        AND Q2 <> 0

    END
    IF @LookupName = 'Lookup.WebsiteAnnualExpense'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q63 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q63 IS NOT NULL

    END
    IF @LookupName = 'Lookup.StaffExpensePercentOfGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q53, 0) / (Q24)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END
    IF @LookupName = 'Lookup.RecallMinPerComplExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q8, 0) * 52 * 60) / (Q14)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END
    IF @LookupName = 'Lookup.PercentOfNewPatientsAttracted'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q17, 0) / ((Q14 * Q16) / 100)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(((ISNULL(Q14, 0) * ISNULL(Q16, 0)) / 100) AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(((ISNULL(Q14, 0) * ISNULL(Q16, 0)) / 100) AS decimal(18, 2))

        END <> 0

    END
    IF @LookupName = 'Lookup.CostOfGoodsPercentOfGrossRev'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q52j, 0) / (Q24)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END
    IF @LookupName = 'Lookup.CLExamPercentTotalExam'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q15b, 0) / (Q14)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q14 IS NOT NULL
        AND Q14 <> 0

    END
    IF @LookupName = 'Lookup.AnnPharmRxPer1000'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q21a, 0) + ISNULL(Q21b, 0) + ISNULL(Q21c, 0) + ISNULL(Q21d, 0)) / (Q12 / 1000)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(ISNULL(Q12, 0) / 1000 AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(ISNULL(Q12, 0) / 1000 AS decimal(18, 2))

        END <> 0

    END
    IF @LookupName = 'Lookup.AnnGrossRevPerFTEOD($000)'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q24, 0) / (Q11 / 2080)) AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear

        WHERE CASE

          WHEN CAST(ISNULL(Q11, 0) / 2080 AS decimal(18, 2)) = 0 THEN 0

          ELSE CAST(ISNULL(Q11, 0) / 2080 AS decimal(18, 2))

        END <> 0

    END


    IF @LookupName = 'Lookup.BestPracticeTOTAL'

    BEGIN

      SET @LookupLogicExists = 1

      SET @query_part = (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q64a', 'Q66k'));

      SET @query_part = 'select source.data from (select ' + @query_part + '* 10 +';

      SET @query_part = @query_part + (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q66n', 'Q66v'));

      SET @query_part = @query_part + ' as data FROM ##InputDataBenchMarkSourceYear ) source where source.data<>0'


      --print @query_part

      EXEC ('Insert into #BenchmarkRawData ' + @query_part)


    END



    IF @LookupName = 'Lookup.BestPracticeSTAFF'

    BEGIN

      SET @LookupLogicExists = 1

      SET @query_part = (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q66a', 'Q66k'));

      SET @query_part = 'select source.data from (select ' + @query_part + '* 10 +';

      SET @query_part = @query_part + (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q66n', 'Q66v'));

      SET @query_part = @query_part + ' as data FROM ##InputDataBenchMarkSourceYear ) source where source.data<>0'

      --print @query_part

      EXEC ('Insert into #BenchmarkRawData ' + @query_part)


    END


    IF @LookupName = 'Lookup.BestPracticeMARKETING'

    BEGIN

      SET @LookupLogicExists = 1

      SET @query_part = (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q65a', 'Q65n'));

      SET @query_part = 'select source.data from (select ' + @query_part + '* 10 ';

      SET @query_part = @query_part + ' as data FROM ##InputDataBenchMarkSourceYear ) source where source.data<>0'

      --print @query_part

      EXEC ('Insert into #BenchmarkRawData ' + @query_part)


    END


    IF @LookupName = 'Lookup.BestPracticeFINANCE'

    BEGIN

      SET @LookupLogicExists = 1

      SET @query_part = (SELECT
        [dbo].[FN_GenerateSelectQueryForColumnRange]('Q64a', 'Q64o'));

      SET @query_part = 'select source.data from (select ' + @query_part + '* 10 ';

      SET @query_part = @query_part + ' as data FROM ##InputDataBenchMarkSourceYear ) source where source.data<>0'

      --print @query_part

      EXEC ('Insert into #BenchmarkRawData ' + @query_part)


    END


    IF @LookupName = 'Lookup.ExpenseRatioPercentageByCostOfGoods_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q52j AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q52j IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByEquipment_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q55 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q55 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByGenOverhead_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q57 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q57 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByInsurance_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q60 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q60 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByInterest_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q58 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q58 IS NOT NULL

    END


    IF @LookupName = 'Lookup.ExpenseRatioPercentageByMarketing_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q56 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q56 IS NOT NULL

    END


    IF @LookupName = 'Lookup.ExpenseRatioPercentageByOccupancy_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q54 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q54 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByRepairMaintenance_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q59 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q59 IS NOT NULL

    END

    IF @LookupName = 'Lookup.ExpenseRatioPercentageByStaffSalaries_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          Q53 AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q53 IS NOT NULL

    END

    IF @LookupName = 'Lookup.AcctRecAgingPercentage30Days_J'

    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST((ISNULL(Q25, 0) / (Q24 / 12)) * 100 AS decimal(18, 2)) AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q24 IS NOT NULL
        AND Q24 <> 0

    END

    IF @LookupName = 'Lookup.EyewearRxCostOfGoods_J'
    BEGIN

      SET @LookupLogicExists = 1

      INSERT INTO #BenchmarkRawData
        SELECT
          CAST(((ISNULL(Q52a, 0) + ISNULL(Q52b, 0) + ISNULL(Q52c, 0) + ISNULL(Q52d, 0) + ISNULL(Q52e, 0))) AS decimal(18, 2))
          AS BenchmarkCandidate
        FROM ##InputDataBenchMarkSourceYear
        WHERE Q52a IS NOT NULL
        AND Q52b IS NOT NULL
        AND Q52c IS NOT NULL
        AND Q52d IS NOT NULL
        AND Q52e IS NOT NULL

    END

    ----Returning candidate keys on which benchmark will be calculated 

    IF (@LookupLogicExists = 0)
    BEGIN

      SELECT
        -999999999

    END


    IF (@LookupName <> 'Lookup.AcctRecAgingPercentage30Days_J')
      SELECT
        BenchmarkCandidate
      FROM #BenchmarkRawData
      ORDER BY BenchmarkCandidate

    ELSE
      SELECT
        BenchmarkCandidate
      FROM #BenchmarkRawData
      ORDER BY BenchmarkCandidate DESC


    IF (SELECT
        OBJECT_ID('Tempdb..#BenchmarkRawData'))
      IS NOT NULL
      DROP TABLE #BenchmarkRawData

  END TRY



  BEGIN CATCH
    /****************************************************************/
    /* RAISE ERROR							*/
    /****************************************************************/
    DECLARE @ErrorMessage nvarchar(1000),
            @ErrorMessagetext nvarchar(1000),
            @ErrorSeverity int,
            @ErrorState int,
            @ErrorNumber int;

    SELECT
      @ErrorNumber = ERROR_NUMBER(),
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorMessagetext =
                         CASE
                           WHEN ERROR_PROCEDURE() IS NULL THEN 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Sql in Procedure: ' + ISNULL(OBJECT_NAME(@@PROCID), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                           ELSE 'SQLError#: ' + CONVERT(varchar, @ErrorNumber) + ', "' + ERROR_MESSAGE() + '"' + ', Procedure: ' + ISNULL(ERROR_PROCEDURE(), '') + ', Line#: ' + CONVERT(varchar, ERROR_LINE())
                         END,
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessagetext,
    @ErrorSeverity,
    @ErrorState);


    IF (SELECT
        OBJECT_ID('Tempdb..#BenchmarkRawData'))
      IS NOT NULL
      DROP TABLE #BenchmarkRawData


  END CATCH

END

GO


