USE [PPASurvey_DBProd]
GO

/****** Object:  StoredProcedure [dbo].[SP_CalculateBenchMark]    Script Date: 7/26/2017 7:06:46 PM ******/
DROP PROCEDURE [dbo].[SP_CalculateBenchMark]
GO

/****** Object:  StoredProcedure [dbo].[SP_CalculateBenchMark]    Script Date: 7/26/2017 7:06:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-- =============================================
-- Author:Amit Gupta		
-- Create date: 06 July 2017
-- Description:	This Proc Calculate 100 Percentile values on the data points
				for a Metrics and Populate the Lookup table with those values.
				Input Parameters are as Follows:
				@LookupName:Lookup Table for which Benchmark,Percentile Values, has to be calculated
				@Year:The Year for which data Points have to be picked.
				@type:BenchmarkType,If 'F'(Full) then all historical data points  including the @year will be considered
									else IF 'A'(Annual) then Data points only for only that @year will be considered and will be saved in a new table.
11 July 2017:Rahul verma modified Proc to Truncate the Lookup table if it exists already.
					
-- Execution Sample:Exec dbo.SP_CalculateBenchMark 'Lookup.AverageDirectPayExamFee_J','2017','F'
-- =============================================*/
CREATE Procedure [dbo].[SP_CalculateBenchMark](@LookupName sysname, @year varchar(4), @type char(1),@OutputValue int = NULL  OUTPUT)
AS

Begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ uncommitted
Declare @Count int,@Lookupval [decimal](18, 2),@PercentileIndex [decimal](5, 2),@NextRevData Decimal(18,2),@CurRevData Decimal(18,2),@loop int,@val Decimal(18,2)
Declare @lpart int,@rpart int,@strPercentileIndex Varchar(10),@valmax Decimal(18,2),@ind [decimal](5, 2),@Sqltext NVarchar(500)

Declare @Benchmark Table(BenchmarkId Int Identity(1,1) ,TmpPercentileIndex decimal(10, 2) NULL,BenchmarkCandidate Decimal(18,2))
BEGIN TRY
	
	Insert into @Benchmark(BenchmarkCandidate)
	EXEC SP_BenchMarkLogic @LookupName,@year,@type
 
	Select @count=@@ROWCOUNT-- from @Benchmark

	if(@Count=0) 
	BEGIN
	
	--print('No data available to calculate benchmark for table name '+@LookupName)
	SELECT @OutputValue= -1

	RETURN 0
	
	END

	if(@Count=1 AND (SELECT TOP 1 BenchmarkCandidate FROM @Benchmark)=-999999999) 
	BEGIN
	
	--print('Benchmark logic not available')

	SELECT @OutputValue= -2
	
	RETURN 0
	END
		
--Calculating Percentile index
	 Update @Benchmark
	 Set [TmpPercentileIndex]=Cast(cast(BenchmarkId as Decimal(5,2))* @count/100 as Decimal(10,2))
	

	
	IF (SELECT Object_id('Tempdb..#LookupValues'))IS NULL
	Create Table #LookupValues
	 (
	 TempLookupValuesId int,
	 LookupVal Decimal(18,2),
	 [ResponseMin] [decimal](18, 2) NULL,
	 [ResponseMax] [decimal](18, 2) NULL,
	 [Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
	)
 
	 Set @NextRevData=0.0
	 Set @CurRevData=0.0
	 Set @Lookupval=0.0
	 Set @loop=1
	 Set @val=0.0
	 Set @lpart=0
	 Set @rpart=0
 ---Calculating 100 percentile values
	 While @loop<=100
	 Begin
		Select @strPercentileIndex=Cast(TmpPercentileIndex as varchar(10)) from @Benchmark where BenchmarkId=@loop

		select @lpart=left(@strPercentileIndex,CHARINDEX('.',@strPercentileIndex)-1),@rpart=right(@strPercentileIndex,CHARINDEX('.',Reverse(@strPercentileIndex))-1)
		--select @lpart,@rpart
		Select @NextRevData=BenchmarkCandidate from @Benchmark where BenchmarkId=@lpart+1
		If @lpart=0
		Begin
			Select @val=@lpart+(@NextRevData*(cast(@rpart as decimal(5,2))/100))
		End 
	
		Select @CurRevData=BenchmarkCandidate from @Benchmark where BenchmarkId=@lpart

		Select @val=((@CurRevData)+((@NextRevData-@CurRevData)*cast(@rpart as decimal(5,2))/100))
	
		Insert Into #LookupValues(TempLookupValuesId,LookupVal,Percentile,ResponseMax)values(@loop,@val,cast(@loop as Varchar(3))+'.0',@val)

		Select @valmax=ResponseMax From #LookupValues Where TempLookupValuesId=@loop-1
		IF @loop>1
		Begin
			IF @valmax<@val
			Begin
				Update #LookupValues Set ResponseMin=(Select ResponseMax+0.01 From #LookupValues Where TempLookupValuesId=@loop-1) 
				Where TempLookupValuesId=@loop
			End 
			Else
	
				Update #LookupValues Set ResponseMin=(Select ResponseMin From #LookupValues Where TempLookupValuesId=@loop-1) 
				Where TempLookupValuesId=@loop

			End  --End of IF @valmax<@val
		Else

				Update #LookupValues Set ResponseMin = CASE WHEN LookupVal < 0 THEN LookupVal-1 ELSE 0 END where TempLookupValuesId=@loop
				Set @loop=@loop+1
				Set @val=0.0
				Set @CurRevData=0.0
				Set @NextRevData=0.0
		End ----End of @loop>1

--Updating Lookup Labels
		Update #LookupValues
		Set LookupLable=Convert(Varchar(10),TempLookupValuesId)+Case When TempLookupValuesId%100 IN(11,12,13) THEN 'th'
																ELSE 
																	CASE TempLookupValuesId %10 
																	When 1 then 'st'
																	When 2 then 'nd'
																	When 3 then 'rd'
																	Else 'th'
																End
															End
		From #LookupValues

		
--Inserting Benchmark data into existing Lookup Table

		if @type='F'
		begin

		SET @Sqltext= ' TRUNCATE TABLE '+Quotename(@LookupName) +' ; Insert into '+Quotename(@LookupName)+' Select TempLookupValuesId,LookupVal,ResponseMin,ResponseMax,Percentile,LookupLable from #LookupValues'
	

		end
--Inserting Benchmark data after creating a New Lookup Table <@LookupName+@year>
		if @type='A'
		begin
		
		SET @Sqltext=' IF OBJECT_ID ('''+ Quotename(@LookupName+'_'+@year)+''', ''U'') IS NOT NULL drop table '+Quotename(@LookupName+'_'+@year)+' ; Select TempLookupValuesId as RowId ,LookupVal as LookupValue ,ResponseMin as ResponseMin ,ResponseMax as ResponseMax ,Percentile as Percentile ,LookupLable as
		 LookupLable into '+Quotename(@LookupName+'_'+@year) + ' from #LookupValues'


		end

--Inserting Benchmark data into Lookup Table			
					
		Exec Sp_executesql @Sqltext
		
		

		IF (SELECT Object_id('Tempdb..#LookupValues'))IS NOT NULL
			DROP TABLE #LookupValues

	
END TRY

BEGIN CATCH
      /****************************************************************/
      /* RAISE ERROR							*/
          /****************************************************************/
          DECLARE @ErrorMessage     NVARCHAR(1000),
                  @ErrorMessagetext NVARCHAR(1000),
                  @ErrorSeverity    INT,
                  @ErrorState       INT,
                  @ErrorNumber      INT;

          SELECT @ErrorNumber = Error_number(),
                 @ErrorMessage = Error_message(),
                 @ErrorMessagetext = CASE
                                       WHEN Error_procedure() IS NULL THEN 'SQLError#: ' + Convert(VARCHAR, @ErrorNumber) + ', "' + Error_message() + '"' + ', Sql in Procedure: ' + Isnull(Object_name(@@PROCID), '') + ', Line#: ' + Convert(VARCHAR, Error_line())
                                       ELSE 'SQLError#: ' + Convert(VARCHAR, @ErrorNumber) + ', "' + Error_message() + '"' + ', Procedure: ' + Isnull(Error_procedure(), '') + ', Line#: ' + Convert(VARCHAR, Error_line())
                                     END,
                 @ErrorSeverity = Error_severity(),
                 @ErrorState = Error_state();

          RAISERROR (@ErrorMessagetext,
                     @ErrorSeverity,
                     @ErrorState);

         
			IF (SELECT Object_id('Tempdb..#LookupValues'))IS NOT NULL
			DROP TABLE #LookupValues
			
  END CATCH

End

GO


