USE [PPASurvey_DBProd]
GO

alter table [Lookup.AcctRecDaysOutstanding] alter column LookupValue Decimal (18,2);
alter table [Lookup.AcctRecDaysOutstanding] alter column ResponseMin Decimal (18,2);
alter table [Lookup.AcctRecDaysOutstanding] alter column ResponseMax Decimal (18,2);

alter table [Lookup.AnnMedEyeCareVisitPer1000] alter column LookupValue Decimal (18,2);
alter table [Lookup.AnnMedEyeCareVisitPer1000] alter column ResponseMin Decimal (18,2);
alter table [Lookup.AnnMedEyeCareVisitPer1000] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CLGrossProfitMargin] alter column LookupValue Decimal (18,2);
alter table [Lookup.CLGrossProfitMargin] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CLGrossProfitMargin] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CLNewFitsPer100CLExam] alter column LookupValue Decimal (18,2);
alter table [Lookup.CLNewFitsPer100CLExam] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CLNewFitsPer100CLExam] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CLSalesPercentGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.CLSalesPercentGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CLSalesPercentGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CompleteExamsPer100Active] alter column LookupValue Decimal (18,2);
alter table [Lookup.CompleteExamsPer100Active] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CompleteExamsPer100Active] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CompleteExamsPerODHour] alter column LookupValue Decimal (18,2);
alter table [Lookup.CompleteExamsPerODHour] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CompleteExamsPerODHour] alter column ResponseMax Decimal (18,2);

alter table [Lookup.EyewearGrossProfitMargin] alter column LookupValue Decimal (18,2);
alter table [Lookup.EyewearGrossProfitMargin] alter column ResponseMin Decimal (18,2);
alter table [Lookup.EyewearGrossProfitMargin] alter column ResponseMax Decimal (18,2);

alter table [Lookup.EyewearRxPer100ComplExam] alter column LookupValue Decimal (18,2);
alter table [Lookup.EyewearRxPer100ComplExam] alter column ResponseMin Decimal (18,2);
alter table [Lookup.EyewearRxPer100ComplExam] alter column ResponseMax Decimal (18,2);

alter table [Lookup.EyewearSalePercentageOfGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.EyewearSalePercentageOfGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.EyewearSalePercentageOfGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.MedicalEyeCareVisitPercentTotal] alter column LookupValue Decimal (18,2);
alter table [Lookup.MedicalEyeCareVisitPercentTotal] alter column ResponseMin Decimal (18,2);
alter table [Lookup.MedicalEyeCareVisitPercentTotal] alter column ResponseMax Decimal (18,2);

alter table [Lookup.NetIncomePercentGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.NetIncomePercentGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.NetIncomePercentGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.AnnCLSalesPerCLExam] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.AnnCLSalesPerCLExam] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.AnnCLSalesPerCLExam] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.AnnMrktSpendPerComplExam] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.AnnMrktSpendPerComplExam] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.AnnMrktSpendPerComplExam] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.AvgCollectFeeRevPerCompl] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.AvgCollectFeeRevPerCompl] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.AvgCollectFeeRevPerCompl] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ChairCostPerComplExam] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ChairCostPerComplExam] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ChairCostPerComplExam] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ExamFeeNonCL] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ExamFeeNonCL] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ExamFeeNonCL] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ExamFeeSoftLensNOREFITT] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftLensNOREFITT] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftLensNOREFITT] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ExamFeeSoftNewFitMULTIFO] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitMULTIFO] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitMULTIFO] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ExamFeeSoftNewFitSPHERE] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitSPHERE] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitSPHERE] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.ExamFeeSoftNewFitTORIC] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitTORIC] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.ExamFeeSoftNewFitTORIC] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevenuePerCompleteExam] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevenuePerCompleteExam] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevenuePerCompleteExam] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevenuePerODHour] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevenuePerODHour] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevenuePerODHour] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevPerActivePatient] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevPerActivePatient] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevPerActivePatient] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevPerEyewearRx] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevPerEyewearRx] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevPerEyewearRx] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevPerNonODStaffHr] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevPerNonODStaffHr] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevPerNonODStaffHr] alter column ResponseMax$ Decimal (18,2);


alter table [Lookup.CLRefitPercentCLExam] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.CLRefitPercentCLExam] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.CLRefitPercentCLExam] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.CLWearerPercentActivePatients] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.CLWearerPercentActivePatients] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.CLWearerPercentActivePatients] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.DailyDisposableLensPercentSoft] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.DailyDisposableLensPercentSoft] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.DailyDisposableLensPercentSoft] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.HighIndexLensPercentSpecLensRx] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.HighIndexLensPercentSpecLensRx] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.HighIndexLensPercentSpecLensRx] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.MultipleEyewearPurchasePercent] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.MultipleEyewearPurchasePercent] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.MultipleEyewearPurchasePercent] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.NoGlareLensPercentSpecLensRx] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.NoGlareLensPercentSpecLensRx] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.NoGlareLensPercentSpecLensRx] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.PercentExamsProvideWMangCareDis] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.PercentExamsProvideWMangCareDis] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.PercentExamsProvideWMangCareDis] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.PhotochrLensPercentofSpecLensRx] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.PhotochrLensPercentofSpecLensRx] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.PhotochrLensPercentofSpecLensRx] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.SiliconeHydroLensWearPercentSoft] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.SiliconeHydroLensWearPercentSoft] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.SiliconeHydroLensWearPercentSoft] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.SoftMultiFocPercentSoftLens] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.SoftMultiFocPercentSoftLens] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.SoftMultiFocPercentSoftLens] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.SoftToricPercentSoftLens] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.SoftToricPercentSoftLens] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.SoftToricPercentSoftLens] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.PercentPatientsCLExamPurchEyewea] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.PercentPatientsCLExamPurchEyewea] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.PercentPatientsCLExamPurchEyewea] alter column [ResponseMax%] Decimal (18,2);

------------------------------------------------------------------------------------------------------
alter table [Lookup.ManagedCarePercentGrossRev] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.ManagedCarePercentGrossRev] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.ManagedCarePercentGrossRev] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.MonthlySoftLensPercentWearers] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.MonthlySoftLensPercentWearers] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.MonthlySoftLensPercentWearers] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.MrktSpendPercentGrossRev] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.MrktSpendPercentGrossRev] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.MrktSpendPercentGrossRev] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.NewPatientExamPercentTotalExam] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.NewPatientExamPercentTotalExam] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.NewPatientExamPercentTotalExam] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [LookupValue%] Decimal (18,2);
alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [ResponseMin%] Decimal (18,2);
alter table [Lookup.ProgressiveLensAndPresbyopRx] alter column [ResponseMax%] Decimal (18,2);

alter table [Lookup.AnnGrossRevPerFTEOD($000)] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.AnnGrossRevPerFTEOD($000)] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.AnnGrossRevPerFTEOD($000)] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.GrossRevPerSqFt] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.GrossRevPerSqFt] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.GrossRevPerSqFt] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.WebsiteAnnualExpense] alter column LookupValue$ Decimal (18,2);
alter table [Lookup.WebsiteAnnualExpense] alter column ResponseMin$ Decimal (18,2);
alter table [Lookup.WebsiteAnnualExpense] alter column ResponseMax$ Decimal (18,2);

alter table [Lookup.AnnPharmRxPer1000] alter column LookupValue Decimal (18,2);
alter table [Lookup.AnnPharmRxPer1000] alter column ResponseMin Decimal (18,2);
alter table [Lookup.AnnPharmRxPer1000] alter column ResponseMax Decimal (18,2);

alter table [Lookup.AnnStaffHrsPerAnnODHrs] alter column LookupValue Decimal (18,2);
alter table [Lookup.AnnStaffHrsPerAnnODHrs] alter column ResponseMin Decimal (18,2);
alter table [Lookup.AnnStaffHrsPerAnnODHrs] alter column ResponseMax Decimal (18,2);

alter table [Lookup.BestPracticeMARKETING] alter column LookupValue Decimal (18,2);
alter table [Lookup.BestPracticeMARKETING] alter column ResponseMin Decimal (18,2);
alter table [Lookup.BestPracticeMARKETING] alter column ResponseMax Decimal (18,2);

alter table [Lookup.BestPracticeSTAFF] alter column LookupValue Decimal (18,2);
alter table [Lookup.BestPracticeSTAFF] alter column ResponseMin Decimal (18,2);
alter table [Lookup.BestPracticeSTAFF] alter column ResponseMax Decimal (18,2);

alter table [Lookup.BestPracticeTOTAL] alter column LookupValue Decimal (18,2);
alter table [Lookup.BestPracticeTOTAL] alter column ResponseMin Decimal (18,2);
alter table [Lookup.BestPracticeTOTAL] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CLExamPercentTotalExam] alter column LookupValue Decimal (18,2);
alter table [Lookup.CLExamPercentTotalExam] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CLExamPercentTotalExam] alter column ResponseMax Decimal (18,2);

alter table [Lookup.CostOfGoodsPercentOfGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.CostOfGoodsPercentOfGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.CostOfGoodsPercentOfGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.NonRefrFeePercentGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.NonRefrFeePercentGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.NonRefrFeePercentGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.OccupancyExpensePercentGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.OccupancyExpensePercentGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.OccupancyExpensePercentGrossRev] alter column ResponseMax Decimal (18,2);

alter table [Lookup.PercentOfNewPatientsAttracted] alter column LookupValue Decimal (18,2);
alter table [Lookup.PercentOfNewPatientsAttracted] alter column ResponseMin Decimal (18,2);
alter table [Lookup.PercentOfNewPatientsAttracted] alter column ResponseMax Decimal (18,2);

alter table [Lookup.RecallMinPerComplExam] alter column LookupValue Decimal (18,2);
alter table [Lookup.RecallMinPerComplExam] alter column ResponseMin Decimal (18,2);
alter table [Lookup.RecallMinPerComplExam] alter column ResponseMax Decimal (18,2);

alter table [Lookup.StaffExpensePercentOfGrossRev] alter column LookupValue Decimal (18,2);
alter table [Lookup.StaffExpensePercentOfGrossRev] alter column ResponseMin Decimal (18,2);
alter table [Lookup.StaffExpensePercentOfGrossRev] alter column ResponseMax Decimal (18,2);