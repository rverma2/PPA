USE [PPASurvey_DBProd]
GO
/****** Object:  Table [dbo].[ES_2012_Non-OD_Staff_Salary_Increases]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_2012_Non-OD_Staff_Salary_Increases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_2012_Non-OD_Staff_Salary_Increases](
	[0.0] [varchar](50) NULL,
	[1-1.9%] [varchar](50) NULL,
	[2-2.9%] [varchar](50) NULL,
	[3-3.9%] [varchar](50) NULL,
	[4-4.9%] [varchar](50) NULL,
	[5-6.9%] [varchar](50) NULL,
	[7-9.9%] [varchar](50) NULL,
	[10%_or_more] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Actual_Exam_Interval_of_Eyeglasses-only_Wearers_by_Exam_Interval_Recommended_by_OD_pcnt_of_practices]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Actual_Exam_Interval_of_Eyeglasses-only_Wearers_by_Exam_Interval_Recommended_by_OD_pcnt_of_practices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Actual_Exam_Interval_of_Eyeglasses-only_Wearers_by_Exam_Interval_Recommended_by_OD_pcnt_of_practices](
	[Actual_Average_Interval_12-16_months_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_17-21_months_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_22-26_months_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_27-31_months_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_32-36_months_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_Average_Interval_Exam_Interval_Recommended_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_12-16_months_Exam_Interval_Recommended_longer_than_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_17-21_months_Exam_Interval_Recommended_longer_than_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_22-26_months_Exam_Interval_Recommended_longer_than_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_27-31_months_Exam_Interval_Recommended_longer_than_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_32-36_months_Exam_Interval_Recommended_longer_than_12_months] [varchar](50) NULL,
	[Actual_Average_Interval_Average_Interval_Exam_Interval_Recommended_Longet_than_12_months] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Actual_Eyeglasses-only_Patient_Eye_Exam_Interval_pcnt_of_practices]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Actual_Eyeglasses-only_Patient_Eye_Exam_Interval_pcnt_of_practices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Actual_Eyeglasses-only_Patient_Eye_Exam_Interval_pcnt_of_practices](
	[12-16_months] [varchar](50) NULL,
	[17-21_months] [varchar](50) NULL,
	[22-26_months] [varchar](50) NULL,
	[27-31_months] [varchar](50) NULL,
	[32-36_months] [varchar](50) NULL,
	[Average_Interval] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Adult_Internet_Usage_by_Age_2012]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Adult_Internet_Usage_by_Age_2012]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Adult_Internet_Usage_by_Age_2012](
	[Age_18-29_%_of_Adults_Using_Internet] [varchar](50) NULL,
	[Age_30-49_%_of_Adults_Using_Internet] [varchar](50) NULL,
	[Age_50-64__%_of_Adults_Using_Internet] [varchar](50) NULL,
	[Age__65_and_older__%_of_Adults_Using_Internet] [varchar](50) NULL,
	[Age_Total_%_of_Adults_Using_Internet] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Advanced_Lens_Features]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Advanced_Lens_Features]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Advanced_Lens_Features](
	[No-Glare_lenses] [varchar](50) NULL,
	[Photochromic_lenses] [varchar](50) NULL,
	[Polarized_lenses] [varchar](50) NULL,
	[Computer_lenses] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Anti-fog_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Anti-fog_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Anti-fog_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Average_Direct_Pay_Exam_Fee_weighted_average_of_all_exam_types]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Average_Direct_Pay_Exam_Fee_weighted_average_of_all_exam_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Average_Direct_Pay_Exam_Fee_weighted_average_of_all_exam_types](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Average_Frames_Mark_Up]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Average_Frames_Mark_Up]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Average_Frames_Mark_Up](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Bundled_Lens_Packaged_Sales]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Bundled_Lens_Packaged_Sales]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Bundled_Lens_Packaged_Sales](
	[Progressive_Best_package] [varchar](50) NULL,
	[Progressive_Middle_package] [varchar](50) NULL,
	[Progressive_Lowest_package] [varchar](50) NULL,
	[Single_vision_Best_package] [varchar](50) NULL,
	[Single_vision_Middle_package] [varchar](50) NULL,
	[Single_vision_Lowest_package] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Consumer_Expenditure_for_New_Prescription_Eyeglasses]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Consumer_Expenditure_for_New_Prescription_Eyeglasses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Consumer_Expenditure_for_New_Prescription_Eyeglasses](
	[Eyewear_Retail_Price_$351_or_more_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_$251-$350_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_$151-$250_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_$101-$150_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_$100_or_less_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_$351_or_more_$_Sales] [varchar](50) NULL,
	[Eyewear_Retail_Price_$251-$350_$_Sales] [varchar](50) NULL,
	[Eyewear_Retail_Price_$151-$250_$_Sales] [varchar](50) NULL,
	[Eyewear_Retail_Price_$101-$150_$_Sales] [varchar](50) NULL,
	[Eyewear_Retail_Price_$100_or_less_$_Sales] [varchar](50) NULL,
	[Eyewear_Retail_Price_Total_Units] [varchar](50) NULL,
	[Eyewear_Retail_Price_Total_$_Sales] [varchar](50) NULL,
	[Estimated_average_price_paid:_Total_Pairs] [varchar](50) NULL,
	[Estimated_average_price_paid:_With_No-Glare] [varchar](50) NULL,
	[Estimated_average_price_paid:_No_A-R] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Contact_Lens_Management_Best_Practices]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Contact_Lens_Management_Best_Practices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Contact_Lens_Management_Best_Practices](
	[Discount_annual_supply_purchases] [varchar](50) NULL,
	[Have_practice_website_contact_lens_re-order_functionality] [varchar](50) NULL,
	[Achieve_exam_day_dispense-frominventory_ratio_of_35%+_of_patients] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Design_pcnt_of_lens_pairs]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Design_pcnt_of_lens_pairs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Design_pcnt_of_lens_pairs](
	[Bifocal/Trifocal] [varchar](50) NULL,
	[Progressive] [varchar](50) NULL,
	[Single_Vision] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Dispensary_Space_by_Total_Office_Size_Quintiles]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Dispensary_Space_by_Total_Office_Size_Quintiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Dispensary_Space_by_Total_Office_Size_Quintiles](
	[Office_Sq._Ft._Quintile_Smallest_20%] [varchar](50) NULL,
	[Office_Sq._Ft._Quintile_Smallest_Next_20%] [varchar](50) NULL,
	[Office_Sq._Ft._Quintile_Median_20%] [varchar](50) NULL,
	[Office_Sq._Ft._Quintile_Median_Next_20%] [varchar](50) NULL,
	[Office_Sq._Ft._Quintile_Largest_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_Smallest_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_Smallest_Next_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_Median_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_Median_Next_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_Largest_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_%_of_Total_Smallest_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_%_of_Total_Smallest_Next_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_%_of_Total_Median_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_%_of_Total_Median_Next_20%] [varchar](50) NULL,
	[Dispensary_Sq._Ft_%_of_Total_Largest_20%] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Employment_Status_of_U.S._Adults_2012pcnt_employed]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Employment_Status_of_U.S._Adults_2012pcnt_employed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Employment_Status_of_U.S._Adults_2012pcnt_employed](
	[Age_16-19_Total_Adults] [varchar](50) NULL,
	[Age_16-19_Male] [varchar](50) NULL,
	[Age_16-19_Female] [varchar](50) NULL,
	[Age_20-24_TotalAdults] [varchar](50) NULL,
	[Age_20-24_Male] [varchar](50) NULL,
	[Age_20-24_Female] [varchar](50) NULL,
	[Age_25-34_TotalAdults] [varchar](50) NULL,
	[Age_25-34_Male] [varchar](50) NULL,
	[Age_25-34_Female] [varchar](50) NULL,
	[Age_35-44_Totla_Adults] [varchar](50) NULL,
	[Age_35-44_Male] [varchar](50) NULL,
	[Age_35-44_Female] [varchar](50) NULL,
	[Age_45-54_Total_Adults] [varchar](50) NULL,
	[Age_45-54_Male] [varchar](50) NULL,
	[Age_45-54_Female] [varchar](50) NULL,
	[Age_55-64_Total_Adults] [varchar](50) NULL,
	[Age_55-64_male] [varchar](50) NULL,
	[Age_55-64_Female] [varchar](50) NULL,
	[Age_65_and_older_Total_Adults] [varchar](50) NULL,
	[Age_65_and_older_Male] [varchar](50) NULL,
	[Age_65_and_older_Female] [varchar](50) NULL,
	[Age_Total__Total_Adults] [varchar](50) NULL,
	[Age_Total_Male] [varchar](50) NULL,
	[Age_Total_Female] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Eyeglasses_Re-purchase_Cycle_2012]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Eyeglasses_Re-purchase_Cycle_2012]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Eyeglasses_Re-purchase_Cycle_2012](
	[Total_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[Total_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[Total_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[Total_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[Male_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[Male_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[Male_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[Male_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[Female_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[Female_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[Female_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[Female_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[18-34_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[18-34_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[18-34_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[18-34_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[35-44_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[35-44_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[35-44_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[35-44_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[44-54_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[44-54_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[44-54_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[44-54_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[55_and_older_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[55_and_older_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[55_and_older_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[55_and_older_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Re-purchase_Cycle_Months] [varchar](50) NULL,
	[Annual_Household_Income_Less_than_$60,000_Eyeglasses_Pairs_Purchased_million] [varchar](50) NULL,
	[Annual_Household_Income_Less_than_$60,000_Eyeglasses_Wearers_million] [varchar](50) NULL,
	[Annual_Household_Income_Less_than_$60,000_Re-purchase_Cycle_Years] [varchar](50) NULL,
	[Annual_Household_Income_Less_than_$60,000_Re-purchase_Cycle_Months] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Eyeglasses-only_Wearer_OD-Recommended_Exam_Interval_pcnt_of_practices]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Eyeglasses-only_Wearer_OD-Recommended_Exam_Interval_pcnt_of_practices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Eyeglasses-only_Wearer_OD-Recommended_Exam_Interval_pcnt_of_practices](
	[Less_than_12_months_Pre-Presbyope] [varchar](50) NULL,
	[12_months_Pre-Presbyope] [varchar](50) NULL,
	[18_months_Pre-Presbyope] [varchar](50) NULL,
	[18_months_Presbyope] [varchar](50) NULL,
	[24_Months_Pre-Presbyope] [varchar](50) NULL,
	[24_months_Presbyope] [varchar](50) NULL,
	[Other_Pre-Presbyope] [varchar](50) NULL,
	[Other_Presbyope] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Eyewear_Gross_Profit_Margin_pcnt_Performance_Deciles]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Eyewear_Gross_Profit_Margin_pcnt_Performance_Deciles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Eyewear_Gross_Profit_Margin_pcnt_Performance_Deciles](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Frequency_of_Purchase_of_Prescription_Eyewear]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Frequency_of_Purchase_of_Prescription_Eyewear]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Frequency_of_Purchase_of_Prescription_Eyewear](
	[Less_often_than_every_three_years] [varchar](50) NULL,
	[About_every_three_years] [varchar](50) NULL,
	[About_every_two_years] [varchar](50) NULL,
	[About_every_18_months] [varchar](50) NULL,
	[Usually_at_least_once_a_year] [varchar](50) NULL,
	[Estimated_average_interval] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Frequency_of_Staff_Meetings]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Frequency_of_Staff_Meetings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Frequency_of_Staff_Meetings](
	[Weekly_or_more_frequently] [varchar](50) NULL,
	[Every_two_weeks] [varchar](50) NULL,
	[Monthly] [varchar](50) NULL,
	[Monthly_or_more_often_net] [varchar](50) NULL,
	[Every_eight_weeks] [varchar](50) NULL,
	[Quarterly] [varchar](50) NULL,
	[Every_six_months] [varchar](50) NULL,
	[Every_twelve_months] [varchar](50) NULL,
	[Naver] [varchar](50) NULL,
	[Average_number_of_staff_meetings_convened_per_year] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Frequency_of_Updating_Prescription_Eyewear_among_Presbyopes_pcnt_of_presbyopic_eyewear_buyers_able_to_estimate_interval]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Frequency_of_Updating_Prescription_Eyewear_among_Presbyopes_pcnt_of_presbyopic_eyewear_buyers_able_to_estimate_interval]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Frequency_of_Updating_Prescription_Eyewear_among_Presbyopes_pcnt_of_presbyopic_eyewear_buyers_able_to_estimate_interval](
	[Less_often_than_every_five_years] [varchar](50) NULL,
	[Every_3-5_years] [varchar](50) NULL,
	[Every_1-2_years] [varchar](50) NULL,
	[More_than_once_a_year] [varchar](50) NULL,
	[Estimated_average_interval] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Full-Time_Office_Manager_by_Practice_Size]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Full-Time_Office_Manager_by_Practice_Size]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Full-Time_Office_Manager_by_Practice_Size](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Gross_Revenue_per_Non-OD_Staff_Hour_pcnt_formance_Deciles_Cumulative_pcnt_of_Adult_patient]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Gross_Revenue_per_Non-OD_Staff_Hour_pcnt_formance_Deciles_Cumulative_pcnt_of_Adult_patient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Gross_Revenue_per_Non-OD_Staff_Hour_pcnt_formance_Deciles_Cumulative_pcnt_of_Adult_patient](
	[Year_1] [varchar](50) NULL,
	[Year_2] [varchar](50) NULL,
	[Year_3] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection](
	[$356] [varchar](50) NULL,
	[$581] [varchar](50) NULL,
	[$698] [varchar](50) NULL,
	[$823] [varchar](50) NULL,
	[$947] [varchar](50) NULL,
	[$1,106] [varchar](50) NULL,
	[$1,300] [varchar](50) NULL,
	[$1,532] [varchar](50) NULL,
	[$1,852] [varchar](50) NULL,
	[$2,950] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection_]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection_]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Gross_Revenue_per_Square_Foot_by_Practice_Size_for_refrection_](
	[$356] [varchar](50) NULL,
	[$581] [varchar](50) NULL,
	[$698] [varchar](50) NULL,
	[$823] [varchar](50) NULL,
	[$947] [varchar](50) NULL,
	[$1,106] [varchar](50) NULL,
	[$1,300] [varchar](50) NULL,
	[$1,532] [varchar](50) NULL,
	[$1,852] [varchar](50) NULL,
	[$2,950] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Improving_Eyewear_Capture_Rate]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Improving_Eyewear_Capture_Rate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Improving_Eyewear_Capture_Rate](
	[Annual_Eyeglasses_Sales_Annual_Practice_Gross_Revenue_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_Annual_Practice_Gross_Revenue_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_Annual_Practice_Gross_Revenue_$1_million] [varchar](50) NULL,
	[Eyeglasses_Pairs_Annual_Practice_Gross_Revenue_$500,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_Annual_Practice_Gross_Revenue_$750,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_Annual_Practice_Gross_Revenue_$_1_million] [varchar](50) NULL,
	[Eyewear_Capture_Rate_75%_Annual_Eyeglasses_Sales_$500.000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_75%_Annual_Eyeglasses_Sales_$750.000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_75%_Annual_Eyeglasses_Sales_$1_Million] [varchar](50) NULL,
	[Eyewear_Capture_Rate_80%_Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_80%_Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_80%_Annual_Eyeglasses_Sales_$1_Million] [varchar](50) NULL,
	[Eyewear_Capture_Rate_85%_Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_85%_Annual_Eyeglasses_Sales__$750,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_85%_Annual_Eyeglasses_Sales__$1_Million] [varchar](50) NULL,
	[Eyewear_Capture_Rate_95%_Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_95%_Annual_Eyeglasses_Sales_750,000] [varchar](50) NULL,
	[Eyewear_Capture_Rate_95%_Annual_Eyeglasses_Sales_1_million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_Average_Eyewear_Retail_Sale]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_Average_Eyewear_Retail_Sale]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_Average_Eyewear_Retail_Sale](
	[Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Eyeglasses_Pairs_$500,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_$750,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_$1_million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$250_Annual_Eyeglasses_Sales_AGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$250_Annual_Eyeglasses_Sales_AGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$250_Annual_Eyeglasses_Sales_AGR_$1_million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$300_Annual_Eyeglasses_Sales_AGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$300_Annual_Eyeglasses_Sales_AGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$300_Annual_Eyeglasses_Sales_AGR_$1_million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$350_Annual_Eyeglasses_Sales_AGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$350_Annual_Eyeglasses_Sales_AGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sales__$350_Annual_Eyeglasses_Sales_AGR_$1_million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_Eyeglasses_Sales_to_Contact_Lens_Patients]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_Eyeglasses_Sales_to_Contact_Lens_Patients]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_Eyeglasses_Sales_to_Contact_Lens_Patients](
	[Contact_Lens_Exams_Annual_Practice_Gross_Revenue$500,000] [varchar](50) NULL,
	[Contact_Lens_Exams_Annual_Practice_Gross_Revenue$750,000] [varchar](50) NULL,
	[Contact_Lens_Exams_Annual_Practice_Gross_Revenue$_1_million] [varchar](50) NULL,
	[Contact_Lens_Patients_Purchasing_Eyeglasses_Annual_Practice_Gross_Revenue$500,000] [varchar](50) NULL,
	[Contact_Lens_Patients_Purchasing_Eyeglasses_Annual_Practice_Gross_Revenue$750,000] [varchar](50) NULL,
	[Contact_Lens_Patients_Purchasing_Eyeglasses_Annual_Practice_Gross_Revenue$1_million] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_to_Contact_Lens_Patients__Annual_Practice_Gross_Revenue$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_to_Contact_Lens_Patients__Annual_Practice_Gross_Revenue$1_750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_to_Contact_Lens_Patients__Annual_Practice_Gross_Revenue$1_million] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_30%_AESCLP_APGR_$500,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_30%_AESCLP_APGR_$750,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_30%_AESCLP_APGR_$1_Million] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_40%_AESCLP_APGR_$500,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_40%_AESCLP_APGR_$750,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_40%_AESCLP_APGR_$1_Million] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_50%_AESCLP_APGR_$500,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_50%_AESCLP_APGR_$750,000] [varchar](50) NULL,
	[%_of_Contact_Lens_Patients_Purchasing_Eyeglasses_50%_AESCLP_APGR_$1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_High-Index_Lens_pcnt]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_High-Index_Lens_pcnt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_High-Index_Lens_pcnt](
	[Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Eyeglasses_Pairs_$500,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_$750,000] [varchar](50) NULL,
	[Eyeglasses_Pairs_$1_million] [varchar](50) NULL,
	[High-Index_Lens_Pairs_$500,000] [varchar](50) NULL,
	[High-Index_Lens_Pairs_$750,000] [varchar](50) NULL,
	[High-Index_Lens_Pairs_$1_million] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_15%_AES_AGR_$500,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_15%_AES_AGR_$750,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_15%_AES_AGR_$1_million] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$500,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$750,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$1_million] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$500,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$750,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$1_million] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$500,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$750,000] [varchar](50) NULL,
	[High-index_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$1_million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_Multiple_Pair_Sales]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_Multiple_Pair_Sales]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_Multiple_Pair_Sales](
	[Annual_Eyeglasses_Sales1_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales2_$7500,00] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales3_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs1_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs2_$750,000] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs3_$1_million] [varchar](50) NULL,
	[Patients_Purchasing_Eyeglasses1_$500,000] [varchar](50) NULL,
	[Patients_Purchasing_Eyeglasses2_$750,000] [varchar](50) NULL,
	[Patients_Purchasing_Eyeglasses3_$1_million] [varchar](50) NULL,
	[Patients_Purchasing_Multiple_Pairs_10%1_$500,000] [varchar](50) NULL,
	[Patients_Purchasing_Multiple_Pairs_10%2_$750,000] [varchar](50) NULL,
	[Patients_Purchasing_Multiple_Pairs_10%3_$1_million] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_15%_AES_AGR_$500,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_15%_AES_AGR_$750,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_15%_AES_AGR_$!_Million] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_20%_AES_AGR_$500,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_20%_AES_AGR_$750,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_20%_AES_AGR_$!_Million] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_25%_AES_AGR_$500,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_25%_AES_AGR_$750,00] [varchar](50) NULL,
	[%_of_Patients_Purchasing_Multiple_Pairs_25%_AES_AGR_$!_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_No-Glare_Anti-Reflective_Lens_pcnt]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_No-Glare_Anti-Reflective_Lens_pcnt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_No-Glare_Anti-Reflective_Lens_pcnt](
	[Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_sales_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_sales_$750,000] [varchar](50) NULL,
	[Total_Eyeglasses_sales_$1_million] [varchar](50) NULL,
	[No-Glare_Lens_Pairs_$500,000] [varchar](50) NULL,
	[No-Glare_Lens_Pairs_$750,000] [varchar](50) NULL,
	[No-Glare_Lens_Pairs_$1_million] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_40%_AES_AGR_$500,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_40%_AES_AGR_$750,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_40%_AES_AGR_$1_Million] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_60%_AES_AGR_$500,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_60%_AES_AGR_$750,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_60%_AES_AGR_$1_Million] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_70%_AES_AGR_$500,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_70%_AES_AGR_$750,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_70%_AES_AGR_$1_Million] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_80%_AES_AGR_$500,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_80%_AES_AGR_$750,000] [varchar](50) NULL,
	[No-Glare_Lens_%_of_Total_Eyeglasses_Rxes_80%_AES_AGR_$1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_Photochromic_Lens_pcnt]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_Photochromic_Lens_pcnt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_Photochromic_Lens_pcnt](
	[Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Photochromic_Lens_Pairs_$500,000] [varchar](50) NULL,
	[Photochromic_Lens_Pairs_$750,000] [varchar](50) NULL,
	[Photochromic_Lens_Pairs_$1_million] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$500,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$750,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_20%_AES_AGR_$1_Million] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$500,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$750,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_25%_AES_AGR_$1_Million] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$500,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$750,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_30%_AES_AGR_$1_Million] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_35%_AES_AGR_$500,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_35%_AES_AGR_$750,000] [varchar](50) NULL,
	[Photochromic_%_of_Total_Eyeglasses_Rxes_35%_AES_AGR_$1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Increasing_Progressive_Lens_pcnt_of_Presbyopic_Rxes]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Increasing_Progressive_Lens_pcnt_of_Presbyopic_Rxes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Increasing_Progressive_Lens_pcnt_of_Presbyopic_Rxes](
	[Annual_Eyeglasses_Sales_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$750,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$750,000] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$1_million] [varchar](50) NULL,
	[Presbyopic_Lens_Pairs_$500,000] [varchar](50) NULL,
	[Presbyopic_Lens_Pairs_$750,000] [varchar](50) NULL,
	[Presbyopic_Lens_Pairs_$1_million] [varchar](50) NULL,
	[Progressive_Lens_Pairs_$500,000] [varchar](50) NULL,
	[Progressive_Lens_Pairs_$750,000] [varchar](50) NULL,
	[Progressive_Lens_Pairs_$1_million] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_60%_AES_AGR_$500,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_60%_AES_AGR_$750,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_60%_AES_AGR_$1_million] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_70%_AES_AGR_$500,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_70%_AES_AGR_$750,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_70%_AES_AGR_$1_million] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_80%_AES_AGR_$500,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_80%_AES_AGR_$750,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_80%_AES_AGR_$1_million] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_90%_AES_AGR_$500,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_90%_AES_AGR_$750,000] [varchar](50) NULL,
	[Progressive_Lens_%_of_Presbyopic_Lenses_90%_AES_AGR_$1_million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Margin_Increase_on_Eyewear_Gross_Profit]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Margin_Increase_on_Eyewear_Gross_Profit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Margin_Increase_on_Eyewear_Gross_Profit](
	[Annual_Eyeglasses_Sales1_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales2_$7500,00] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales3_$1_million] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$500,000] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$7500,00] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$1_million] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_63%_AEGS_APGR_$500,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_63%_AEGS_APGR_$750,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_63%_AEGS_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_65%_AEGS_APGR_$500,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_65%_AEGS_APGR_$750,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_65%_AEGS_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_67%_AEGS_APGR_$500,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_67%_AEGS_APGR_$750,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_67%_AEGS_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_69%_AEGS_APGR_$500,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_69%_AEGS_APGR_$750,00] [varchar](50) NULL,
	[Average_Eyewear_Gross_Margin_69%_AEGS_APGR_$1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Patient_Defection_on_Practice_Revenue]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Patient_Defection_on_Practice_Revenue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Patient_Defection_on_Practice_Revenue](
	[Annual_Eyeglasses_Sales1_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales2_$7500,00] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales3_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_Buyers_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_Buyers_$750,000] [varchar](50) NULL,
	[Total_Eyeglasses_Buyers_$1_million] [varchar](50) NULL,
	[Annual_Eyewear_Buyers_$500,000] [varchar](50) NULL,
	[Annual_Eyewear_Buyers_$750,000] [varchar](50) NULL,
	[Annual_Eyewear_Buyers_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_Wearers_In_Practice_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_Wearers_In_Practice_$750,000] [varchar](50) NULL,
	[Total_Eyeglasses_Wearers_In_Practice_$1_million] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_28_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_28_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_28_AES_APGR_$_1_Million] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_25_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_25_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_25_AES_APGR_$_1_Million] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_23_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_23_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Interval_Between_Eyewear_Purchases_months_23_AES_APGR_$_1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Pricing_on_Eyewear_Purchase_pcnt_of_patients_saying_statement_describes_completely/very_well]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Pricing_on_Eyewear_Purchase_pcnt_of_patients_saying_statement_describes_completely/very_well]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Pricing_on_Eyewear_Purchase_pcnt_of_patients_saying_statement_describes_completely/very_well](
	[I_always_shop_around_to_get_the_best_prices._Eyewear_Buyers_of:_Optical_Chain] [varchar](50) NULL,
	[I_always_shop_around_to_get_the_best_prices._Eyewear_Buyers_of:_Independent_ECP] [varchar](50) NULL,
	[When_it_comes_to_buying_glasses,_I_think_it_is_worth_paying_a_little_more_to_get_the_best._Option_Chain] [varchar](50) NULL,
	[When_it_comes_to_buying_glasses,_I_think_it_is_worth_paying_a_little_more_to_get_the_best.:_Independent_ECP] [varchar](50) NULL,
	[When_it_comes_to_my_vision,_the_amount_of_money_I_spend_is_not_a_concern._Option_Chain] [varchar](50) NULL,
	[When_it_comes_to_my_vision,_the_amount_of_money_I_spend_is_not_a_concern._Independent_ECP] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Reducing_Interval_Between_Eyewear_Purchases]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Reducing_Interval_Between_Eyewear_Purchases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Reducing_Interval_Between_Eyewear_Purchases](
	[Active_Patients_$500,000] [varchar](50) NULL,
	[Active_Patients_$7500,00] [varchar](50) NULL,
	[Active_Patients_$1_million] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_2.5%_ARL_APGR_$500,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_2.5%_ARL_APGR_$750,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_2.5%_ARL_APGR_$_1_Million] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_5.0%_ARL_APGR_$500,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_5.0%_ARL_APGR_$750,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_5.0%_ARL_APGR_$_1_Million] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_7.5%_ARL_APGR_$500,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_7.5%_ARL_APGR_$750,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_7.5%_ARL_APGR_$_1_Million] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_10.0%_ARL_APGR_$500,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_10.0%_ARL_APGR_$750,000] [varchar](50) NULL,
	[Annual_Patient_Defection_Rate_10.0%_ARL_APGR_$_1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Impact_of_Retail_Price_Increase_on_Eyewear_Sales]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Impact_of_Retail_Price_Increase_on_Eyewear_Sales]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Impact_of_Retail_Price_Increase_on_Eyewear_Sales](
	[Annual_Eyeglasses_Sales1_$500,000] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales2_$7500,00] [varchar](50) NULL,
	[Annual_Eyeglasses_Sales3_$1_million] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$500,000] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$7500,00] [varchar](50) NULL,
	[Annual_Eyewear_Gross_Profit_$1_million] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$500,000] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$7500,00] [varchar](50) NULL,
	[Total_Eyeglasses_Pairs_$1_million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AES_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AES_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AES_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AES_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AES_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AEGP_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AEGP_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$238.35_AEGP_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AEGP_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AEGP_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$249.7_AEGP_APGR_$1_Million] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AEGP_APGR_$500,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AEGP_APGR_$750,000] [varchar](50) NULL,
	[Average_Eyewear_Retail_Sale_$261.05_AEGP_APGR_$1_Million] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Income_and_Expenditures_of_U.S._Households_2011]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Income_and_Expenditures_of_U.S._Households_2011]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Income_and_Expenditures_of_U.S._Households_2011](
	[Income_Quintile_Lowest_20%_Before_Tax_Annual_Income_range] [varchar](50) NULL,
	[Income_Quintile_Lowest_Next_20%_Before_Tax_Annual_Income_range] [varchar](50) NULL,
	[Income_Quintile_Median_20%_Before_Tax_Annual_Income_range] [varchar](50) NULL,
	[Income_Quintile_Median_Next_20%_Before_Tax_Annual_Income_range] [varchar](50) NULL,
	[Income_Quintile_Highest_20%_Before_Tax_Annual_Income_range] [varchar](50) NULL,
	[Income_Quintile_Lowest_20%_Before_Tax_Annual_Income_average] [varchar](50) NULL,
	[Income_Quintile_Lowest_Next_20%_Before_Tax_Annual_Income_average] [varchar](50) NULL,
	[Income_Quintile_Median_20%_Before_Tax_Annual_Income_average] [varchar](50) NULL,
	[Income_Quintile_Median_Next_20%_Before_Tax_Annual_Income_average] [varchar](50) NULL,
	[Income_Quintile_Highest_20%_Before_Tax_Annual_Income_average] [varchar](50) NULL,
	[Income_Quintile_Lowest_20%_%_of_income] [varchar](50) NULL,
	[Income_Quintile_Lowest_Next_20%_%_of_income] [varchar](50) NULL,
	[Income_Quintile_Median_20%_%_of_income] [varchar](50) NULL,
	[Income_Quintile_Median_Next_20%_%_of_income] [varchar](50) NULL,
	[Income_Quintile_Highest_20%_%_of_income] [varchar](50) NULL,
	[Income_Quintile_Lowest_20%_%_of_Expenditures] [varchar](50) NULL,
	[Income_Quintile_Lowset_Next_20%_%_of_Expenditures] [varchar](50) NULL,
	[Income_Quintile_Median_20%_%_of_Expenditures] [varchar](50) NULL,
	[Income_Quintile_Median_Next_20%_%_of_Expenditures] [varchar](50) NULL,
	[Income_Quintile_Highest_20%_%_of_Expenditures] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Independent_ECP_Eyeglasses_Sales_2009-2012]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Independent_ECP_Eyeglasses_Sales_2009-2012]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Independent_ECP_Eyeglasses_Sales_2009-2012](
	[Units_million_frames_2009] [varchar](50) NULL,
	[Units_million_frames_2010] [varchar](50) NULL,
	[Units_million_frames_2011] [varchar](50) NULL,
	[Units_million_frames_2012] [varchar](50) NULL,
	[Units_million_frames_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Units_million_lenses_2009] [varchar](50) NULL,
	[Units_million_lenses_2010] [varchar](50) NULL,
	[Units_million_lenses_2011] [varchar](50) NULL,
	[Units_million_lenses_2012] [varchar](50) NULL,
	[Units_million_lenses_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Dollars_$million_Frames_2009] [varchar](50) NULL,
	[Dollars_$million_Frames_2010] [varchar](50) NULL,
	[Dollars_$million_Frames_2011] [varchar](50) NULL,
	[Dollars_$million_Frames_2012] [varchar](50) NULL,
	[Dollars_$million_Frames_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Dollars_$million_Lenses_2009] [varchar](50) NULL,
	[Dollars_$million_Lenses_2010] [varchar](50) NULL,
	[Dollars_$million_Lenses_2011] [varchar](50) NULL,
	[Dollars_$million_Lenses_2012] [varchar](50) NULL,
	[Dollars_$million_Lenses_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Dollars_$million_Total_2009] [varchar](50) NULL,
	[Dollars_$million_Total_2010] [varchar](50) NULL,
	[Dollars_$million_Total_2011] [varchar](50) NULL,
	[Dollars_$million_Total_2012] [varchar](50) NULL,
	[Dollars_$million_Total_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Frames_2009] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Frames_2010] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Frames_2011] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Frames_2012] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Frames_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Lenses_2009] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Lenses_2010] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Lenses_2011] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Lenses_2012] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Lenses_2009-2012_Annual_%_change] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Total_2009] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Total_2010] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Total_2011] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Total_2012] [varchar](50) NULL,
	[Average_Revenue_per_Pair_Total_2009-2012_Annual_%_change] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Independent_ECP_Eyewear_Capture_Rate]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Independent_ECP_Eyewear_Capture_Rate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Independent_ECP_Eyewear_Capture_Rate](
	[%_of_eye_exams_2009] [varchar](50) NULL,
	[%_of_eye_exams_2010] [varchar](50) NULL,
	[%_of_eye_exams_2011] [varchar](50) NULL,
	[%_of_eye_exams_2012] [varchar](50) NULL,
	[%_of_frames_unit_sales_2009] [varchar](50) NULL,
	[%_of_frames_unit_sales_2010] [varchar](50) NULL,
	[%_of_frames_unit_sales_2011] [varchar](50) NULL,
	[%_of_frames_unit_sales_2012] [varchar](50) NULL,
	[%_of_lens_unit_sales_2009] [varchar](50) NULL,
	[%_of_lens_unit_sales_2010] [varchar](50) NULL,
	[%_of_lens_unit_sales_2011] [varchar](50) NULL,
	[%_of_lens_unit_sales_2012] [varchar](50) NULL,
	[Frames_capture_rate_2009] [varchar](50) NULL,
	[Frames_capture_rate_2010] [varchar](50) NULL,
	[Frames_capture_rate_2011] [varchar](50) NULL,
	[Frames_capture_rate_2012] [varchar](50) NULL,
	[Lens_capture_rate_2009] [varchar](50) NULL,
	[Lens_capture_rate_2010] [varchar](50) NULL,
	[Lens_capture_rate_2011] [varchar](50) NULL,
	[Lens_capture_rate_2012] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Independent_ECP_Market_Share]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Independent_ECP_Market_Share]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Independent_ECP_Market_Share](
	[TGR_2009] [varchar](50) NULL,
	[TGR_2010] [varchar](50) NULL,
	[TGR_2011] [varchar](50) NULL,
	[TGR_2012] [varchar](50) NULL,
	[TGR_2013] [varchar](50) NULL,
	[Eyewear_Sales_2009] [varchar](50) NULL,
	[Eyewear_Sales_2010] [varchar](50) NULL,
	[Eyewear_Sales_2011] [varchar](50) NULL,
	[Eyewear_Sales_2012] [varchar](50) NULL,
	[Eyewear_Sales_2013] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Independent_ECP_Market_Share_and_Capture_Rate]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Independent_ECP_Market_Share_and_Capture_Rate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Independent_ECP_Market_Share_and_Capture_Rate](
	[Sell_46.0%_of_spectacle_lens_units] [varchar](50) NULL,
	[Sell_43.6%_of_frames_units] [varchar](50) NULL,
	[Sell_52%_of_soft_lens_units] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Independent_ECP_Revenue_Trends_pcnt_change_versus_year_ago]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Independent_ECP_Revenue_Trends_pcnt_change_versus_year_ago]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Independent_ECP_Revenue_Trends_pcnt_change_versus_year_ago](
	[TGR_Dec_2009] [varchar](50) NULL,
	[TGR_Dec_2010] [varchar](50) NULL,
	[TGR_Dec_2011] [varchar](50) NULL,
	[TGR_2012] [varchar](50) NULL,
	[TGR_June_2013] [varchar](50) NULL,
	[Eyewear_Sales_Dec_2009] [varchar](50) NULL,
	[Eyewear_Sales_Dec_2010] [varchar](50) NULL,
	[Eyewear_Sales_Dec_2011] [varchar](50) NULL,
	[Eyewear_Sales_2012] [varchar](50) NULL,
	[Eyewear_Sales_June_2013] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Leisure_Activities_of_American_Adults]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Leisure_Activities_of_American_Adults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Leisure_Activities_of_American_Adults](
	[Watching_TV_%_of_Adults_Participating] [varchar](50) NULL,
	[Watching_TV_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Home_improvement/repair_%_of_Adults_Participating] [varchar](50) NULL,
	[Home_improvement/repair_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Exercise_walking_%_of_Adults_Participating] [varchar](50) NULL,
	[Exercise_walking_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Reading_books_%_of_Adults_Participating] [varchar](50) NULL,
	[Reading_books_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Flower_gardening_%_of_Adults_Participating] [varchar](50) NULL,
	[Flower_gardening_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Vegetable_gardening_%_of_Adults_Participating] [varchar](50) NULL,
	[Vegetable_gardening_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Cooking_for_fun_%_of_Adults_Participating] [varchar](50) NULL,
	[Cooking_for_fun_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Jogging/running_%_of_Adults_Participating] [varchar](50) NULL,
	[Jogging/running_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Sewing_%_of_Adults_Participating] [varchar](50) NULL,
	[Sewing_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Bicycling_%_of_Adults_Participating] [varchar](50) NULL,
	[Bicycling_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Fishing_%_of_Adults_Participating] [varchar](50) NULL,
	[Fishing_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Golf_%_of_Adults_Participating] [varchar](50) NULL,
	[Golf_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Hunting_%_of_Adults_Participating] [varchar](50) NULL,
	[Hunting_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Boating_%_of_Adults_Participating] [varchar](50) NULL,
	[Boating_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Target_shooting_%_of_Adults_Participating] [varchar](50) NULL,
	[Target_shooting_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Painting/drawing_%_of_Adults_Participating] [varchar](50) NULL,
	[Painting/drawing_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Woodworking_%_of_Adults_Participating] [varchar](50) NULL,
	[Woodworking_2nd_Pair_Eyewear_Needs] [varchar](50) NULL,
	[Tennis_%_of_ults_articipating] [varchar](50) NULL,
	[Tennis_2nd_Pair_Eyewear_eds] [varchar](50) NULL,
	[Archery_%_of_ults_articipating] [varchar](50) NULL,
	[Archery_2nd_Pair_eyewear_needs] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Mark-up_on_Spectacle_Lenses_by_Type_pcnt_of_independent_optometrists]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Mark-up_on_Spectacle_Lenses_by_Type_pcnt_of_independent_optometrists]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Mark-up_on_Spectacle_Lenses_by_Type_pcnt_of_independent_optometrists](
	[2.5x_or_less_single_vision_Polycarb] [varchar](50) NULL,
	[2.75x_single_vision_Polycarb] [varchar](50) NULL,
	[3.0x_single_vision_Polycarb] [varchar](50) NULL,
	[3.25x_single_vision_Polycarb] [varchar](50) NULL,
	[3.5x_or_more_single_vision_Polycarb] [varchar](50) NULL,
	[total_single_vision_Polycarb] [varchar](50) NULL,
	[average_single_vision_Polycarb] [varchar](50) NULL,
	[2.5x_or_less_single_vision_Polycarb_AR] [varchar](50) NULL,
	[2.75x_single_vision_Polycarb_AR] [varchar](50) NULL,
	[3.0x_single_vision_Polycarb_AR] [varchar](50) NULL,
	[3.25x_single_vision_Polycarb_AR] [varchar](50) NULL,
	[3.5x_or_more_single_vision_Polycarb_AR] [varchar](50) NULL,
	[total_single_vision_Polycarb_AR] [varchar](50) NULL,
	[average_single_vision_Polycarb_AR] [varchar](50) NULL,
	[2.5x_or_less_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[2.75x_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[3.0x_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[3.25x_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[3.5x_or_more_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[total_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[average_single_vision_Poly_AR__Hi-Index] [varchar](50) NULL,
	[2.5x_or_less_Progressive_Polycarb] [varchar](50) NULL,
	[2.75x_Progressive_Polycarb] [varchar](50) NULL,
	[3.0x_Progressive_Polycarb] [varchar](50) NULL,
	[3.25x_Progressive_Polycarb] [varchar](50) NULL,
	[3.5x_or_more_Progressive_Polycarb] [varchar](50) NULL,
	[total_Progressive_Polycarb] [varchar](50) NULL,
	[average_Progressive_Polycarb] [varchar](50) NULL,
	[2.5x_or_less_Progressive_Polycarb_AR] [varchar](50) NULL,
	[2.75x_Progressive_Polycarb_AR] [varchar](50) NULL,
	[3.0x_Progressive_Polycarb_AR] [varchar](50) NULL,
	[3.25x_Progressive_Polycarb_AR] [varchar](50) NULL,
	[3.5x_or_more_Progressive_Polycarb_AR] [varchar](50) NULL,
	[total_Progressive_Polycarb_AR] [varchar](50) NULL,
	[average_Progressive_Polycarb_AR] [varchar](50) NULL,
	[2.5x_or_less_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[2.75x_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[3.0x_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[3.25x_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[3.5x_or_more_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[total_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[average_Progressive_Hi-Index_AR] [varchar](50) NULL,
	[2.5x_or_less_Progressive_Photochromic_AR] [varchar](50) NULL,
	[2.75x_Progressive_Photochromic_AR] [varchar](50) NULL,
	[3.0x_Progressive_Photochromic_AR] [varchar](50) NULL,
	[3.25x_Progressive_Photochromic_AR] [varchar](50) NULL,
	[3.5x_or_more_Progressive_Photochromic_AR] [varchar](50) NULL,
	[total_Progressive_Photochromic_AR] [varchar](50) NULL,
	[average_Progressive_Photochromic_AR] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Material_pcnt_of_lens_pairs]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Material_pcnt_of_lens_pairs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Material_pcnt_of_lens_pairs](
	[High-Index] [varchar](50) NULL,
	[2%_Glass] [varchar](50) NULL,
	[Plastic] [varchar](50) NULL,
	[Polycarbonate] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Need_Assessment_During_Eye_Exams_pcnt_of_independent_ECP_patients_who_were_asked_about]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Need_Assessment_During_Eye_Exams_pcnt_of_independent_ECP_patients_who_were_asked_about]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Need_Assessment_During_Eye_Exams_pcnt_of_independent_ECP_patients_who_were_asked_about](
	[Current_vision_problems] [varchar](50) NULL,
	[Avocational_vision_needs] [varchar](50) NULL,
	[Occupational_vision_needs] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Number_of_Pairs_of_Prescription_Eyeglasses_Regularly_Used]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Number_of_Pairs_of_Prescription_Eyeglasses_Regularly_Used]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Number_of_Pairs_of_Prescription_Eyeglasses_Regularly_Used](
	[Total_One] [varchar](50) NULL,
	[Total_Two] [varchar](50) NULL,
	[Total_Three_more] [varchar](50) NULL,
	[Total_Total] [varchar](50) NULL,
	[Male_One] [varchar](50) NULL,
	[Male_Two] [varchar](50) NULL,
	[Male_Three_or_more] [varchar](50) NULL,
	[Male_Total] [varchar](50) NULL,
	[Female_One] [varchar](50) NULL,
	[Female_Two] [varchar](50) NULL,
	[Female_Three_or_more] [varchar](50) NULL,
	[Female_Total] [varchar](50) NULL,
	[Age_18-34_One] [varchar](50) NULL,
	[Age_18-34_Two] [varchar](50) NULL,
	[Age_18-34_Three_or_more] [varchar](50) NULL,
	[Age_18-34_Total] [varchar](50) NULL,
	[Age_35-44_One] [varchar](50) NULL,
	[Age_35-44_Two] [varchar](50) NULL,
	[Age_35-34_Three_or_more] [varchar](50) NULL,
	[Age_35-34_Total] [varchar](50) NULL,
	[Age_45-54_One] [varchar](50) NULL,
	[Age_45-54_Two] [varchar](50) NULL,
	[Age_45-54_Three_or_more] [varchar](50) NULL,
	[Age_Total] [varchar](50) NULL,
	[Age_55_and_older_One] [varchar](50) NULL,
	[Age_55_and_older_Two] [varchar](50) NULL,
	[Age_55_and_older_Three_or_more] [varchar](50) NULL,
	[Age_55_and_older_Total] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Occupations_of_U.S._Labor_Force_2012]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Occupations_of_U.S._Labor_Force_2012]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Occupations_of_U.S._Labor_Force_2012](
	[Business_management/education/healthcare_Million] [varchar](50) NULL,
	[Business_management/education/healthcare_Million_%_of_Total] [varchar](50) NULL,
	[Service_workers_Million] [varchar](50) NULL,
	[Service_workers_%_of_Total] [varchar](50) NULL,
	[Construction/productions/installation/repair_Million] [varchar](50) NULL,
	[Construction/productions/installation/repair_%_of_Total] [varchar](50) NULL,
	[Transportation_Million] [varchar](50) NULL,
	[Transportation_%_of_Total] [varchar](50) NULL,
	[Farming/fishing_Million] [varchar](50) NULL,
	[Farming/fishing_%_of_Total] [varchar](50) NULL,
	[Total_Million] [varchar](50) NULL,
	[Total_%_of_Total] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Office_Square_Footage]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Office_Square_Footage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Office_Square_Footage](
	[Total_Square_Footage_Median_Total] [varchar](50) NULL,
	[Total_Square_Footage_Median_one] [varchar](50) NULL,
	[Total_Square_Footage_Median_two] [varchar](50) NULL,
	[Total_Square_Footage_Median_three_or_more] [varchar](50) NULL,
	[Total_Square_Footage_Averge_Total] [varchar](50) NULL,
	[Total_Square_Footage_Average_one] [varchar](50) NULL,
	[Total_Square_Footage_Average_two] [varchar](50) NULL,
	[Total_Square_Footage_Average_three_or_more] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageMedian_total] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageMedian_one] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageMedian_two] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageMedian_three_or_more] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageAverage_total] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageAverage_one] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageAverage_two] [varchar](50) NULL,
	[Optical_Dispensary_Square_FootageAverage_three_or_more] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageMedian_total] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageMedian_one] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageMedian_two] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageMedian_three_or_more] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageAverage_total] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageAverage_one] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageAverage_two] [varchar](50) NULL,
	[Optical_Dispensary_%_of_Total_Square_FootageAverage_three_or_more] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Patients_Reasons_for_Choosing_Lenses_with_No-Glare_Treatments_pcnt_of_No-Glare_lens_buyers]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Patients_Reasons_for_Choosing_Lenses_with_No-Glare_Treatments_pcnt_of_No-Glare_lens_buyers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Patients_Reasons_for_Choosing_Lenses_with_No-Glare_Treatments_pcnt_of_No-Glare_lens_buyers](
	[To_end_glare] [varchar](50) NULL,
	[To_prevent_reflections] [varchar](50) NULL,
	[To_improve_night_vision] [varchar](50) NULL,
	[To_reduce_eye_fatigue] [varchar](50) NULL,
	[To_improve_overall_vision_by_letting_more_light_through] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_pcnt_of_Contact_Lens_Patients_Purchasing_Eyewear_pcntformance_Deciles]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_pcnt_of_Contact_Lens_Patients_Purchasing_Eyewear_pcntformance_Deciles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_pcnt_of_Contact_Lens_Patients_Purchasing_Eyewear_pcntformance_Deciles](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_pcnt_of_Patients_Pre-appointed_pcntformance_Deciles__among_offices_pre-appointing]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_pcnt_of_Patients_Pre-appointed_pcntformance_Deciles__among_offices_pre-appointing]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_pcnt_of_Patients_Pre-appointed_pcntformance_Deciles__among_offices_pre-appointing](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_pcnt_of_Pre-appointed_Patients_Having_Exams__within_3_months_pcntformance_Deciles]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_pcnt_of_Pre-appointed_Patients_Having_Exams__within_3_months_pcntformance_Deciles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_pcnt_of_Pre-appointed_Patients_Having_Exams__within_3_months_pcntformance_Deciles](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Percent_of_Patients_Dispensed_from_Inventory_by_Inventory_Size]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Percent_of_Patients_Dispensed_from_Inventory_by_Inventory_Size]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Percent_of_Patients_Dispensed_from_Inventory_by_Inventory_Size](
	[Under_100] [varchar](50) NULL,
	[100-149] [varchar](50) NULL,
	[150-249] [varchar](50) NULL,
	[250-399] [varchar](50) NULL,
	[400_or_more] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Pertsonalized,_Digitally_Surfaced_Lens_Usage_Deciles__pcnt_of_Eyewear_Rxes]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Pertsonalized,_Digitally_Surfaced_Lens_Usage_Deciles__pcnt_of_Eyewear_Rxes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Pertsonalized,_Digitally_Surfaced_Lens_Usage_Deciles__pcnt_of_Eyewear_Rxes](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Polarized_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Polarized_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Polarized_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Polycarbonate_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Polycarbonate_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Polycarbonate_Lens_Usage_Deciles_pcnt_of_Eyewear_Rxes](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Second_Pair_Discounts]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Second_Pair_Discounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Second_Pair_Discounts](
	[%_of_independent_Ods_offering_second_pair_discount] [varchar](50) NULL,
	[Discount_offered:Less_than_20%] [varchar](50) NULL,
	[Discount_offered:20%] [varchar](50) NULL,
	[Discount_offered:21-49%] [varchar](50) NULL,
	[Discount_offered:50%] [varchar](50) NULL,
	[Average_%_discount] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Soft_Lens_Inventory_Requirements]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Soft_Lens_Inventory_Requirements]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Soft_Lens_Inventory_Requirements](
	[Practice_Annual_Gross_Revenue_$350,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$350,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$500,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$500,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$650,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$650,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$800,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$800,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$1,000,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$1,000,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$1,200,000_Median_Monthly_Contact_Lens_Exams] [varchar](50) NULL,
	[Practice_Annual_Gross_Revenue_$1,200,000_Soft_Lens_Inventory_Requirement] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Spectacle_Lens_Mark_Ups]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Spectacle_Lens_Mark_Ups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Spectacle_Lens_Mark_Ups](
	[Spectacle_Lens_Mark-Ups_Polycarbonate] [varchar](50) NULL,
	[Spectacle_Lens_Mark-Ups_Polycarbonate_anti_reflective] [varchar](50) NULL,
	[Spectacle_Lens_High_index_anti_reflective] [varchar](50) NULL,
	[Progressive_Lenses_Mark-Ups_Polycarbonate] [varchar](50) NULL,
	[Progressive_Lenses_Mark-Ups_Polycarbonate_anti_reflective] [varchar](50) NULL,
	[Progressive_Lenses_High_index_anti_reflective] [varchar](50) NULL,
	[Progressive_Lenses_Polycarbonate__photochromic] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Spectacle_Lens_Penetration_Index_Index_100_Average_Usage]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Spectacle_Lens_Penetration_Index_Index_100_Average_Usage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Spectacle_Lens_Penetration_Index_Index_100_Average_Usage](
	[Male_No-Glare] [varchar](50) NULL,
	[Male_Photochromic] [varchar](50) NULL,
	[Female__No-Glare] [varchar](50) NULL,
	[Female_Photochromic] [varchar](50) NULL,
	[Age_18-34_No-Glare] [varchar](50) NULL,
	[Age_18-34_Photochromic] [varchar](50) NULL,
	[Age_35-44_No-Glare] [varchar](50) NULL,
	[Age_35-44_Photochromic] [varchar](50) NULL,
	[Age_45-54_No-Glare] [varchar](50) NULL,
	[Age_45-54_Photochromic] [varchar](50) NULL,
	[Age_55_and_older_No-Glare] [varchar](50) NULL,
	[Age_55_and_older_Photochromic] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Spectacle_Lens_Purchaser_Demographics_pcnt_of_spectacle_lens_units_by_buyer_characteristics]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Spectacle_Lens_Purchaser_Demographics_pcnt_of_spectacle_lens_units_by_buyer_characteristics]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Spectacle_Lens_Purchaser_Demographics_pcnt_of_spectacle_lens_units_by_buyer_characteristics](
	[55_and_older_Total
Lenses] [varchar](50) NULL,
	[55_and_older_Single_Vision] [varchar](50) NULL,
	[55_and_older_Bifocal/Trifocal] [varchar](50) NULL,
	[55_and_older_Progressive] [varchar](50) NULL,
	[45-54_Total_Lenses] [varchar](50) NULL,
	[45-54_Single_Vision] [varchar](50) NULL,
	[45-54_Bifocal/Trifocal] [varchar](50) NULL,
	[45-54_Progressive] [varchar](50) NULL,
	[44_or_younger_Total_Lenses] [varchar](50) NULL,
	[44_or_younger_Single_Vision] [varchar](50) NULL,
	[44_or_younger_Bifocal/Trifocal] [varchar](50) NULL,
	[44_or_younger_Progressive] [varchar](50) NULL,
	[Total_Total_Lenses] [varchar](50) NULL,
	[Total_Single_Vision] [varchar](50) NULL,
	[Total_Age_Bifocal/Trifocal] [varchar](50) NULL,
	[Total_Age_Progressive] [varchar](50) NULL,
	[Annual_Household_Income$60,000_and_over_Total_Lenses] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Single_Vision] [varchar](50) NULL,
	[Annual_Household_Income_60,000_and_over_Bifocal/Trifocal] [varchar](50) NULL,
	[Annual_Household_Income_$60,000_and_over_Progressive] [varchar](50) NULL,
	[Annual_Household_Income_Under_$60,000_Total_Lenses] [varchar](50) NULL,
	[Annual_Household_Income_Under_$60,000_Single_Vision] [varchar](50) NULL,
	[Annual_Household_Income_Under_$60,000_Bifocal/Trifocal] [varchar](50) NULL,
	[Annual_Household_Income_Under_$60,000_Progressive] [varchar](50) NULL,
	[Total_Annual_Household_Income_Total_Lenses] [varchar](50) NULL,
	[Total_Annual_Household_Income_Single_Vision] [varchar](50) NULL,
	[Total_Annual_Household_Income__Bifocal/Trifocal] [varchar](50) NULL,
	[Total_Annual_Household_Income_Progressive] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Staff_Bonus_pcnt_of_Salary]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Staff_Bonus_pcnt_of_Salary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Staff_Bonus_pcnt_of_Salary](
	[Less_than_2.0%] [varchar](50) NULL,
	[2.0%_to_3.4%] [varchar](50) NULL,
	[3.5%_to_4.9%] [varchar](50) NULL,
	[5.0%_to_9.9%] [varchar](50) NULL,
	[10.0%_to_14.9%] [varchar](50) NULL,
	[15%_or_more] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Staff_Hourly_and_Annual_Salaries_by_Position_2009]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Staff_Hourly_and_Annual_Salaries_by_Position_2009]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Staff_Hourly_and_Annual_Salaries_by_Position_2009](
	[Office_Manager_Average_Hourly_Salary] [varchar](50) NULL,
	[Office_Manager_median_Hourly_Salary] [varchar](50) NULL,
	[Office_Manager_Median_Annual_Salary] [varchar](50) NULL,
	[Optometric_Assistant_Average_Hourly_Salary] [varchar](50) NULL,
	[Optometric_Assistant_Median_Hourly_Salary] [varchar](50) NULL,
	[Optometric_Assistant_Median_Annual_Salary] [varchar](50) NULL,
	[Contact_Lens_Technician_Average_Hourly_Salary] [varchar](50) NULL,
	[Contact_Lens_Technician_Median_Hourly_Salary] [varchar](50) NULL,
	[Contact_Lens_Technician_Median_Annual_Salary] [varchar](50) NULL,
	[Optician/Frames_Stylist_Average_Hourly_Salary] [varchar](50) NULL,
	[Optician/Frames_Stylist_median_Hourly_Salary] [varchar](50) NULL,
	[Optician/Frames_Stylist_Median_Annual_Salary] [varchar](50) NULL,
	[Lab_Manager/Technician_Average_Hourly_Salary] [varchar](50) NULL,
	[Lab_Manager/Technician_Median_Hourly_Salary] [varchar](50) NULL,
	[Lab_Manager/Technician_Median_Annual_Salary] [varchar](50) NULL,
	[Receptionist_Average_Hourly_Salary] [varchar](50) NULL,
	[Receptionist_Median_Hourly_Salary] [varchar](50) NULL,
	[Receptionist_Median_Annual_Salary] [varchar](50) NULL,
	[Bookkeeper_Average_Hourly_Salary] [varchar](50) NULL,
	[Bookkeeper_Median_Hourly_Salary] [varchar](50) NULL,
	[Bookkeeper_Median_Annual_Salary] [varchar](50) NULL,
	[Insurance_Clerk_Stylist_Average_Hourly_Salary] [varchar](50) NULL,
	[Insurance_Clerk_Stylist_Median_Hourly_Salary] [varchar](50) NULL,
	[Insurance_Clerk_Stylist_Median_Annual_Salary] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Staff_Members_per_OD__By_Decile]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Staff_Members_per_OD__By_Decile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Staff_Members_per_OD__By_Decile](
	[5th] [varchar](50) NULL,
	[15th] [varchar](50) NULL,
	[25th] [varchar](50) NULL,
	[35th] [varchar](50) NULL,
	[45th] [varchar](50) NULL,
	[50th] [varchar](50) NULL,
	[55th] [varchar](50) NULL,
	[65th] [varchar](50) NULL,
	[75th] [varchar](50) NULL,
	[85th] [varchar](50) NULL,
	[95th] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Staff_Turnover_2008-2013]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Staff_Turnover_2008-2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Staff_Turnover_2008-2013](
	[Turnover_ratio_weighted_July_2008] [varchar](50) NULL,
	[Turnover_ratio_weighted_February_2011] [varchar](50) NULL,
	[Turnover_ratio_weighted_July_2012] [varchar](50) NULL,
	[Turnover_ratio_weighted_May_2013] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Time_Spent_Watching_Television_by_Age_2011]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Time_Spent_Watching_Television_by_Age_2011]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Time_Spent_Watching_Television_by_Age_2011](
	[15-19_Weekdays_Hours_per_day] [varchar](50) NULL,
	[15-19_Weekends_Hours_per_day] [varchar](50) NULL,
	[15-19_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[20-24_Weekdays_Hours_per_day] [varchar](50) NULL,
	[20-24_Weekends_Hours_per_day] [varchar](50) NULL,
	[20-24_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[25-34_Weekdays_Hours_per_day] [varchar](50) NULL,
	[25-34_Weekends_Hours_per_day] [varchar](50) NULL,
	[25-34_Weekday_%_of_Leisure_Time_WatchingTV] [varchar](50) NULL,
	[35-44_Weekdays_Hours_per_day] [varchar](50) NULL,
	[35-44_Weekends_Hours_per_day] [varchar](50) NULL,
	[35-44_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[45-54_Weekdays_Hours_per_day] [varchar](50) NULL,
	[45-54_Weekends_Hours_per_day] [varchar](50) NULL,
	[45-54_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[55-64_Weekdays_Hours_per_day] [varchar](50) NULL,
	[55-64_Weekends_Hours_per_day] [varchar](50) NULL,
	[55-64_Weekday__%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[65-74_Weekdays_Hours_per_day] [varchar](50) NULL,
	[65-74_Weekends_Hours__per_day] [varchar](50) NULL,
	[65-74_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL,
	[75_and_older_Weekdays_Hours_per_day] [varchar](50) NULL,
	[75_and_older_Weekends_Hours_per_day] [varchar](50) NULL,
	[75_and_older_Weekday_%_of_Leisure_Time_WatchingV] [varchar](50) NULL,
	[Total_15_and_older_Weekdays_Hours_per_day] [varchar](50) NULL,
	[Total_15_and_older_Weekends_Hours_per_day] [varchar](50) NULL,
	[Total_15_and_older_Weekday_%_of_Leisure_Time_Watching_TV] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Total_OD_Source_of_Revenue_2011]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Total_OD_Source_of_Revenue_2011]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Total_OD_Source_of_Revenue_2011](
	[Direct_from_patients] [varchar](50) NULL,
	[Vision_Insurance_plans] [varchar](50) NULL,
	[Private_medical_insurance] [varchar](50) NULL,
	[GovernmentMedicare/Medicaid] [varchar](50) NULL,
	[Other] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Vision_Insurance_Coverage_among_Eyeglasses_Wearers_by_Age]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Vision_Insurance_Coverage_among_Eyeglasses_Wearers_by_Age]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Vision_Insurance_Coverage_among_Eyeglasses_Wearers_by_Age](
	[Vision_insur_by_age_19-24] [varchar](50) NULL,
	[Vision_insur_by_age_25-30] [varchar](50) NULL,
	[Vision_insur_by_age_31-40] [varchar](50) NULL,
	[Vision_insur_by_age_41-45] [varchar](50) NULL,
	[Vision_insur_by_age_46-50] [varchar](50) NULL,
	[Vision_insur_by_age_51-64] [varchar](50) NULL,
	[Vision_insur_by_age_65and_older] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Vision_Problems_of_Eyeglasses_Wearers_pcnt_expcntiencing_problem]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Vision_Problems_of_Eyeglasses_Wearers_pcnt_expcntiencing_problem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Vision_Problems_of_Eyeglasses_Wearers_pcnt_expcntiencing_problem](
	[Sensitive_to_bright_sunlight] [varchar](50) NULL,
	[Night_vision_difficulties] [varchar](50) NULL,
	[Teary_eyes] [varchar](50) NULL,
	[Red_eyes] [varchar](50) NULL,
	[Dry_eyes] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ES_Weekly_Office_Hours_by_Practice_Size]    Script Date: 7/26/2017 5:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Weekly_Office_Hours_by_Practice_Size]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ES_Weekly_Office_Hours_by_Practice_Size](
	[less_tha_$524] [varchar](50) NULL,
	[$525-$576] [varchar](50) NULL,
	[$677-$800] [varchar](50) NULL,
	[$801-$917] [varchar](50) NULL,
	[$918-$1063] [varchar](50) NULL,
	[$1064-$1246] [varchar](50) NULL,
	[$1247-$1450] [varchar](50) NULL,
	[$1451-$1700] [varchar](50) NULL,
	[$1700-$2132] [varchar](50) NULL,
	[more_than_$2133] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ES_Frames_Inventory_Guidelines]    Script Date: 8/9/2017 3:04:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ES_Frames_Inventory_Guidelines]') AND type in (N'U'))
BEGIN
CREATE TABLE  [ES_Frames_Inventory_Guidelines] (
[Median_annual_frams_inventory_turnover_$500,000] varchar(50) NULL, 
[Median_annual_frams_inventory_turnover_$800,000] varchar(50) NULL, 
[Median_annual_frams_inventory_turnover_$1.1_million] varchar(50) NULL, 
[Median_annual_frams_inventory_turnover_$1.4_million] varchar(50) NULL, 
[Median_annual_frams_inventory_turnover_$2_million] varchar(50) NULL 
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO



