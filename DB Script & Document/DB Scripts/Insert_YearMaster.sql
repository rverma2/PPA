USE [PPASurvey_DBProd]
Go

SET IDENTITY_INSERT [YearMaster] ON

INSERT INTO [YearMaster] (YearId, ActiveYear, PreviousActiveYear, CreationDate, IsActive) VALUES (5,'2011', '2010', GETDATE(), 1)

INSERT INTO [YearMaster] (YearId, ActiveYear, PreviousActiveYear, CreationDate, IsActive) VALUES (6,'2012', '2011', GETDATE(), 1)

INSERT INTO [YearMaster]  (YearId, ActiveYear, PreviousActiveYear, CreationDate, IsActive) VALUES (7,'2013', '2012', GETDATE(), 1)



SET IDENTITY_INSERT [YearMaster] OFF
GO