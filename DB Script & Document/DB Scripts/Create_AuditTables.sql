USE [PPASurvey_DBProd]
GO
/****** Object:  Table [dbo].[ExecutionDetail]    Script Date: 7/26/2017 6:17:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExecutionDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExecutionDetail](
	[ExecutionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ExecutionSummaryId] [int] NULL,
	[ExecutionDetailStartDate] [datetime] NULL,
	[ExecutionDetailEndDate] [datetime] NULL,
	[ExecutionDetailLookupTable] [nvarchar](150) NULL,
	[ExecutionDetailStatus] [char](1) NULL,
	[ExecutionErrorDescription] [varchar](1000) NULL,
	[ExecutionDetailLookupRecordCount] [tinyint] NULL,
 CONSTRAINT [PK_ExecutionDetail] PRIMARY KEY CLUSTERED 
(
	[ExecutionDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExecutionSummary]    Script Date: 7/26/2017 6:17:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExecutionSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExecutionSummary](
	[ExecutionSummaryId] [int] IDENTITY(1,1) NOT NULL,
	[ExecutionSummaryStartDate] [datetime] NULL,
	[ExecutionSummaryEndDate] [datetime] NULL,
	[BenchmarkYear] [varchar](4) NULL,
	[TargetBenchmarkCount] [smallint] NULL,
	[PopulatedBenchmarkCount] [smallint] NULL,
	[ExecutionSummaryStatus] [char](1) NULL,
	[ExecutionBenchmarkType] [char](1) NULL,
 CONSTRAINT [PK_ExecutionSummary] PRIMARY KEY CLUSTERED 
(
	[ExecutionSummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
