USE [PPASurvey_DBProd]
GO
/****** Object:  Table [dbo].[Lookup.AcctRecAgingPercentage30Days_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.AcctRecAgingPercentage30Days_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.AcctRecAgingPercentage30Days_J](
	[RowId] [tinyint] NULL,
	[LookupValue] [decimal](10, 2) NULL,
	[ResponseMin] [decimal](10, 2) NULL,
	[ResponseMax] [decimal](10, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.AnnualOccupancyCostperSquareFoot_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.AnnualOccupancyCostperSquareFoot_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.AnnualOccupancyCostperSquareFoot_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lookup.averageframesmarkup_j]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lookup.averageframesmarkup_j]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lookup.averageframesmarkup_j](
	[rowid] [tinyint] NOT NULL,
	[lookupvalue] [decimal](18, 2) NULL,
	[responsemin] [decimal](18, 2) NULL,
	[responsemax] [decimal](18, 2) NULL,
	[percentile] [decimal](5, 1) NULL,
	[lookuplable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ComputerLensesPercentofEyeWearRxes_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ComputerLensesPercentofEyeWearRxes_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ComputerLensesPercentofEyeWearRxes_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByCostOfGoods_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByCostOfGoods_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByCostOfGoods_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByEquipment_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByEquipment_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByEquipment_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByGenOverhead_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByGenOverhead_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByGenOverhead_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByInsurance_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByInsurance_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByInsurance_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByInterest_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByInterest_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByInterest_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByMarketing_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByMarketing_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByMarketing_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByOccupancy_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByOccupancy_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByOccupancy_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByRepairMaintenance_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByRepairMaintenance_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByRepairMaintenance_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.ExpenseRatioPercentageByStaffSalaries_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.ExpenseRatioPercentageByStaffSalaries_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.ExpenseRatioPercentageByStaffSalaries_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 4) NULL,
	[ResponseMin] [decimal](18, 4) NULL,
	[ResponseMax] [decimal](18, 4) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.EyewearRxCostOfGoods_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.EyewearRxCostOfGoods_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.EyewearRxCostOfGoods_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.FramesAvgWholesaleCostPerFrame_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.FramesAvgWholesaleCostPerFrame_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.FramesAvgWholesaleCostPerFrame_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.OpticalDispensaryPercentOfTotalOfficeSpace_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.OpticalDispensaryPercentOfTotalOfficeSpace_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.OpticalDispensaryPercentOfTotalOfficeSpace_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.OtherProductSales_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.OtherProductSales_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.OtherProductSales_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentofCompleteEyeExamsByCLExams_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentofCompleteEyeExamsByCLExams_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentofCompleteEyeExamsByCLExams_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentofCompleteEyeExamsByEyeGlass_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentofCompleteEyeExamsByEyeGlass_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentofCompleteEyeExamsByEyeGlass_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentofCompleteEyeExamsByHealthyEyeExams_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentofCompleteEyeExamsByHealthyEyeExams_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentofCompleteEyeExamsByHealthyEyeExams_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentOfGrossRevenueAllHealthVisionPlans_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentOfGrossRevenueAllHealthVisionPlans_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentOfGrossRevenueAllHealthVisionPlans_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentOfGrossRevenueDirectPatientPayments_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentOfGrossRevenueDirectPatientPayments_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentOfGrossRevenueDirectPatientPayments_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentOfGrossRevenueFromMedicarePayments_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentOfGrossRevenueFromMedicarePayments_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentOfGrossRevenueFromMedicarePayments_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PercentOfGrossRevenueVSPPayments_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PercentOfGrossRevenueVSPPayments_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PercentOfGrossRevenueVSPPayments_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.PrescriptionSunwearPercentofEyeWearRxes_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.PrescriptionSunwearPercentofEyeWearRxes_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.PrescriptionSunwearPercentofEyeWearRxes_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lookup.RGPLensWearerPercentOfCLWeares_J]    Script Date: 7/26/2017 6:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lookup.RGPLensWearerPercentOfCLWeares_J]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Lookup.RGPLensWearerPercentOfCLWeares_J](
	[RowId] [tinyint] NOT NULL,
	[LookupValue] [decimal](18, 2) NULL,
	[ResponseMin] [decimal](18, 2) NULL,
	[ResponseMax] [decimal](18, 2) NULL,
	[Percentile] [decimal](5, 1) NULL,
	[LookupLable] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
